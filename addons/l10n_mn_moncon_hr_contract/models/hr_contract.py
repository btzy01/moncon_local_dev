# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError
from odoo import exceptions
import time
import logging
import datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)


class Contract(models.Model):
    _inherit = 'hr.contract'

    @api.multi
    def _fix_wage_fields(self):
        for obj in self:
            if obj.env.user.has_group("l10n_mn_moncon_hr_contract.group_fix_wage_user"):
                obj.fix_wage_field = True
            else:
                obj.fix_wage_field = False

    time_salary = fields.Float('Үндсэн цагийн цалин')
    kpi_time_salary = fields.Float('Хувьсах цагийн цалин')
    food_addition = fields.Float('Toki нэмэгдэл', help="If field addition give to employee, please check it")
    field_addition = fields.Float('Field additon', help="If field addition give to employee, please check it")
    country_addition = fields.Float('Country additon', help="If country addition give to employee, please check it")
    project_leading_addition = fields.Float('Project leading addition', help="If project leading addition give to employee, please check it")
    major_addition = fields.Float('Major addition')
    worked_year_addition = fields.Float('Worked year addition')
    kpi_salary = fields.Float('KPI Salary')
    fuel_number = fields.Char('Fuel number', help="If fuel number give to employee, please check it")
    fuel_addition = fields.Float('Fuel addition', help="If fuel addition give to employee, please check it")
    show_fuel_field = fields.Boolean(string='Show Cancel Approve Button?', compute='_show_fuel_fields')
    fix_wage_field = fields.Boolean(compute='_fix_wage_fields', string='Fix Wage')
    attachment_add = fields.Float('Хавсарны нэмэгдэл')

    @api.multi
    def _show_fuel_fields(self):
        for obj in self:
            if obj.env.user.has_group("hr_payroll.group_hr_payroll_manager"):
                obj.show_fuel_field = True
            else:
                obj.show_fuel_field = False


class HrContractWageHistory(models.Model):
    _inherit = 'hr.contract.wage.history'

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')
    regulation_type_name = fields.Char(string='Тушаалын төрөл')


class WorkingHoursChangeHistory(models.Model):
    _inherit = 'working.hours.change.history'

    regulation_id = fields.Many2one('hr.regulation', string=u'Тушаал')
