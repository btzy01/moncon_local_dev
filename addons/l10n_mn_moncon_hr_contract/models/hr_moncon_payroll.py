# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError
from odoo import exceptions
import time
import logging
import datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)

class HrMonconPayslip(models.Model):
    _inherit = 'moncon.kpi.line'

    payslip_id = fields.Many2one('hr.payslip')


class HrMonconPayslip(models.Model):
    _inherit = 'hr.payslip'

    kpi_lines = fields.One2many('moncon.kpi.line', 'payslip_id', string='Kpi employee lines')
    employee_register = fields.Char(related='employee_id.ssnid', string='Employee Register', readonly=True, index=True)

    @api.multi
    def action_import_kpi_line(self):
        for obj in self:
            kpi_ids = self.env['moncon.kpi.line'].search(
                [('period_id', '=', obj.period_id.id), ('employee_id', '=', obj.employee_id.id)])
            if not kpi_ids:
                return False
            elif kpi_ids.kpi_line_state_form != 'done':
                raise ValidationError(_('KPI-н үнэлгээ батлагдаж дуусаагүй байна!'))
            else:
                kpi_ids.write({'payslip_id': obj.id})


class HrMonconPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    @api.multi
    def action_import_kpi(self):
        for obj in self:
            for line in obj.slip_ids:
                line.action_import_kpi_line()






