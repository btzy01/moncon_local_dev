# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _
from lxml import etree
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from datetime import datetime
import time

class payment_cert_reject_note(models.Model):
    _name = "payment.cert.reject.note"
    _description = "Payment Certificate Reject Note"

    note = fields.Text(string="Note", required=True)
    
    @api.multi
    def reject(self):
        self.ensure_one()
        context = self.env.context.copy()
        active_ids = context and context.get('active_ids', [])
        payment_cert = self.env['payment.certificate'].browse(active_ids)
        payment_cert.message_post(body=u'Буцаасан шалтгаан: '+self.note)
        
        if payment_cert.state == 'reviewed':
            return payment_cert.action_to_service_head_return(self.note)
        elif payment_cert.state == 'posted_financial_ananlyst':
            return payment_cert.action_to_financial_analyst_return(self.note)
        elif payment_cert.state =='send_project_leader':
            return payment_cert.action_to_project_leader_return(self.note)
        elif payment_cert.state == 'send_director':
            return payment_cert.action_to_director_return(self.note)
        elif payment_cert.state == 'recieving_primary_document':
            return payment_cert.action_to_primary_document_receipt_return(self.note)
        