# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time

from odoo import api, fields, models, _
from odoo.exceptions import Warning as UserError
import odoo.addons.decimal_precision as dp

class ContractTerm(models.Model):
    _inherit = 'contract.term'

    payment_id = fields.Many2one('payment.certificate', 'Connected Payment', compute='compute_payment_id')
    new_payment_id = fields.Many2one('payment.certificate', 'Connected Payment')

    @api.multi
    def action_to_create_payment(self):

        partner_bank_id = None
        if self.contract_id.partner_id.bank_ids:
            for acc in self.contract_id.partner_id.bank_ids:
                partner_bank_id = acc.id

        val = {'allow_payment_types': '1',
               'payment_term_id': self.product_id.id,
               'contract_id': self.contract_id.id,
               'project_id': self.contract_id.project_id.id if self.contract_id.project_id else None,
               'department_id': self.contract_id.department_id.id if self.contract_id.department_id else None,
               'partner_id': self.contract_id.partner_id.id,
               'currency_id': self.contract_id.currency_id.id,
               'currency_amount': self.contract_id.amount_without_tax,
               'payment_type': '8',
               'other_amount_without_tax': self.contract_id.amount_with_tax,
               'total_amount': self.contract_id.amount_with_tax,
               'amount_without_tax': self.contract_id.amount_without_tax,
               'note': self.contract_id.name,
               'after_handover_rate': self.contract_id.after_handover_rate,
               'after_handover_amount': self.contract_id.after_handover_amount,
               'warranty_rate': self.contract_id.pledge_rate,
               'warranty_amount': self.contract_id.pledge_amount,
               'current_perf_subtotal': self.sub_total,
               'contract_tax_id': self.tax_id.id,
               'tax_amount': self.tax_amount,
               'company_bank_id': self.contract_id.project_id.journal_id.id if self.contract_id.project_id else self.contract_id.department_id.journal_id.id,
               'expense_category': self.contract_id.analytic_account_id.id,
               'partner_bank_id': partner_bank_id
               }

        new_payment_id = self.env['payment.certificate'].create(val)

        if self.contract_id.procurement_employee:
            if self.contract_id.procurement_employee.user_id:
                self.env['payment.certificate'].add_follower('payment.certificate', new_payment_id.id,
                                                             self.contract_id.procurement_employee.user_id.partner_id)
            else:
                raise UserError((u'Энэ гэрээний худалдан авалтын ажилтны Холбоотой хэрэглэгч талбар нь хоосон байна.'))

        self.new_payment_id = new_payment_id

        val = {'payment_information': 'Гэрээний гүйцэтгэл',
               'before_amount': 0,
               'current_amount': self.sub_total if not self.tax_id else self.amount,
               'total_amount': (0 + self.sub_total) if not self.tax_id else self.amount,
               'payment_information_id': new_payment_id.id
               }
        new_payment_id.payment_information_line_ids.create(val)

        # Татваргүй суутгалууд
        vals1 = {}
        for ded1 in self.env['contract.deductions'].search([('is_without_tax', '=', True)]):
            vals1 = {
                'payment_cert_id1': new_payment_id.id,
                'deductions_ids': ded1.id,
                'deductions_total': 0,
                'deductions_before': 0,
                'deductions_report': 0,
            }
            new_payment_id.deductions_no_tax_line_ids.create(vals1)

        # Татвартай суутгалууд
        print 'Татвартай суутгалууд: '
        vals2 = {}
        for ded2 in self.env['contract.deductions'].search([('is_without_tax', '=', False)]):
            vals2 = {
                'payment_cert_id2': new_payment_id.id,
                'deductions_ids': ded2.id,
                'deductions_total': 0,
                'deductions_before': 0,
                'deductions_report': 0,
            }
            new_payment_id.deductions_line_ids.create(vals2)

        form_res = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'view_payment_certificate_form')
        form_id = form_res and form_res[1] or False
        tree_res = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'view_payment_certificate_tree')
        tree_id = tree_res and tree_res[1] or False

        return {
            'name': _('Payment Certificate'),
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'payment.certificate',
            'res_id': new_payment_id.id,
            'view_id': False,
            'views': [(form_id, 'form'), (tree_id, 'tree')],
            'type': 'ir.actions.act_window',
        }

    @api.multi
    def compute_payment_id(self):
        for obj in self:
            if obj.new_payment_id:
                obj.payment_id = obj.new_payment_id.id
            elif obj.perf_id.rel_payment_id:
                obj.payment_id = obj.perf_id.rel_payment_id.id
            elif obj.pledge_release_id.payment_id:
                obj.payment_id = obj.pledge_release_id.payment_id.id
            else:
                return False
