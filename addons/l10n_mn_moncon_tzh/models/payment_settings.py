# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api

class PaymentType(models.Model):
	_name = 'payment.type'

	name = fields.Selection([('1','Inventory order page'),
                              ('2','Cutsoms VAT'),
                              ('3','Next calculations appointments'),
                              ('4','Project other'),
                              ('5','Operating costs'),
                              ('6','Not operating costs'),
                              ('7','Other project'),
                              ('8','Notification of payment of the contract')], string='Payment type', required=True)
	required_document = fields.Many2many('payment.document', 'payment_type_doc_rel1','type_id','doc_id','Payment Document')

class PaymentDocument(models.Model): 
	_name = 'payment.document'

	name = fields.Char('Payment Document', required=True)

class hr_department(models.Model):
	_inherit = 'hr.department'

	abbreviated_name = fields.Char('Abbreviated name')