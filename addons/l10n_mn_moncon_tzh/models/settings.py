# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _

class PaymentType(models.Model):
	_name = 'payment.type'

	name = fields.Char('Payment type', required=True)
	payment_document = fields.Many2many('payment.document', 'payment_type_doc_rel','type_id','doc_id','Payment Document')


class PaymentDocument(models.Model): 
	_name = 'payment.document'

	name = fields.Char('Payment Document', required=True)
