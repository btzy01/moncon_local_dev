# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import datetime
import math
import openerp.addons.decimal_precision as dp

from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _

class ContractPerformance(models.Model):
	_inherit = 'contract.performance'

	rel_payment_id = fields.Many2one('payment.certificate', 'Related Payment')

	@api.multi
	def action_create_payment_cert(self):
		for obj in self:
			current_total_amount = 0
			before_perf_total = 0
			
			if obj.work_types == 'work':
				for perf in obj.work_performance_ids:
					current_total_amount += perf.contract_performance_price
					before_perf_total += perf.before_performance_price 
			elif obj.work_types == 'goods':
				for perf in obj.goods_performance_ids:
					current_total_amount += perf.contract_performance_price
					before_perf_total += perf.before_performance_price
					
			for term in obj.contract_id.contract_term_ids:
				if term.product_id.is_advance and term.payment_id and term.payment_id.state in ['recieving_primary_document','finished']:
					before_perf_total += term.payment_id.payment_amount
					
			
			term = obj.env['contract.term'].search([('contract_id','=',obj.contract_id.id), ('product_id','=',obj.payment_terms.id)])
			if not term:
				raise ValidationError(_('This contract term is not present in contract: %s') % obj.payment_terms.name)
			
			term = term[0]
			
			val={'contract_performance_id': obj.id,
				'allow_payment_types': '1',
				'payment_term_id': obj.payment_terms.id,
				'contract_id':obj.contract_id.id,
				'project_id':obj.project_id.id if obj.project_id else None,
				'department_id':obj.department_id.id if obj.department_id else None,
				'partner_id': obj.partner_id.id,
				'currency_id': obj.currency_id.id,
				'currency_amount': obj.contract_id.amount_without_tax,
				'payment_type': '8',
				'amount_without_tax': obj.contract_id.amount_without_tax,
				'note': obj.contract_id.name,
				'contract_tax_id': term.tax_id.id,
				'after_handover_rate': obj.contract_id.after_handover_rate,
				'after_handover_amount': obj.contract_id.after_handover_amount,
				'warranty_rate': obj.contract_id.pledge_rate,
				'warranty_amount': obj.contract_id.pledge_amount,
				'expense_category': obj.contract_id.analytic_account_id.id,
				'company_bank_id': obj.project_id.journal_id.id if obj.project_id else (obj.department_id.journal_id.id if obj.department_id else None)
				}
	
			new_payment_id = obj.env['payment.certificate'].create(val)
			
			if obj.contract_id.procurement_employee:
				if obj.contract_id.procurement_employee.user_id:
					self.env['payment.certificate'].add_follower('payment.certificate', new_payment_id.id, obj.contract_id.procurement_employee.user_id.partner_id)
				else:
					raise UserError((u'Энэ гэрээний худалдан авалтын ажилтны Холбоотой хэрэглэгч талбар нь хоосон байна.'))
			
			val={'payment_information': 'Гэрээний гүйцэтгэл',
				 'before_amount': before_perf_total,
				 'current_amount': current_total_amount,
				 'total_amount': before_perf_total + current_total_amount,
				 'payment_information_id': new_payment_id.id
				 }
			new_payment_id.payment_information_line_ids.create(val)
			
			#Татваргүй суутгалууд
			vals1 = {}
			for ded1 in obj.deductions_line_ids:
				if ded1.deductions_ids.is_without_tax:
					
					vals1 = {
						'payment_cert_id1': new_payment_id.id,
						'deductions_ids': ded1.deductions_ids.id,
						'deductions_total': ded1.deductions_total,
						'deductions_before': ded1.deductions_before,
						'deductions_report': ded1.deductions_report,
					}
					new_payment_id.deductions_no_tax_line_ids.create(vals1)
			
			#Татвартай суутгалууд
			vals2 = {}
			for ded2 in obj.deductions_line_ids:
				if not ded2.deductions_ids.is_without_tax:
					vals2 = {
						'payment_cert_id2': new_payment_id.id,
						'deductions_ids': ded2.deductions_ids.id,
						'deductions_total': ded2.deductions_total,
						'deductions_before': ded2.deductions_before,
						'deductions_report': ded2.deductions_report,
					}
					new_payment_id.deductions_line_ids.create(vals2)
						
			obj.write({'rel_payment_id': new_payment_id.id})
			
			form_res = obj.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'view_payment_certificate_form')
			form_id = form_res and form_res[1] or False
			tree_res = obj.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'view_payment_certificate_tree')
			tree_id = tree_res and tree_res[1] or False
			
			return {
				'name': _('Payment Certificate'),
				'view_type': 'form',
				'view_mode': 'form,tree',
				'res_model': 'payment.certificate',
				'res_id': new_payment_id.id,
				'view_id': False,
				'views': [(form_id, 'form'), (tree_id, 'tree')],
				'type': 'ir.actions.act_window',
			}

	@api.multi
	def show_payment_certificate(self):
	  action = self.env.ref('l10n_mn_moncon_tzh.action_payment_certificate').read()[0]
	  action['views'] = [(self.env.ref('l10n_mn_moncon_tzh.view_payment_certificate_form').id, 'form')]
	  action['res_id'] = self.rel_payment_id.id
	  return action
	 
