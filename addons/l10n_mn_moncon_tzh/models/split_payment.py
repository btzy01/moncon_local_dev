# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _
import time
import datetime
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError

class split_payment(models.TransientModel):
    _name = "split.payment"
    _description = "Split Payment"
    
    pay_amount = fields.Float('Payment Amount', required=True, readonly=True)
    split_lines = fields.One2many('split.payment.line', 'wizard_id', 'Split Line')

    @api.model
    def default_get(self, fields):
        payout_ids = self._context.get('active_ids', [])
        active_model = self._context.get('active_model')
        res = super(split_payment, self).default_get(fields)
        if len(payout_ids) != 1:
            raise ValidationError(_('Please select one payment.'))
        
        payout = self.env['payout.list'].browse(payout_ids)
        if payout.state == 'paid':
            raise ValidationError(_('You cannot split payment which is in \'Paid\' state.'))
        
        res.update(pay_amount=payout.amount_with_tax)
        
        return res
    
    @api.multi
    def action_split(self):
        for wizard in self:
            context = self._context
            payout_ids = context.get('active_ids', [])
            
            sum = 0
            for line in wizard.split_lines:
                sum += line.amount
            if round(wizard.pay_amount,2) != round(sum,2):
                raise ValidationError(_('Split amount is not equal to payment amount.'))
            
            payout = self.env['payout.list'].browse(payout_ids)
            
            rate = payout.mnt_amount/payout.amount_with_tax
            first_line = False
            for line in wizard.split_lines:
                if not first_line:
                    payout.write({'amount_with_tax': line.amount, 'mnt_amount': line.amount*rate})
                    first_line = True
                else:
                    val= {
                        'name': self.env['ir.sequence'].get('payout.list'),
                        'payment_id': payout.payment_id.id if payout.payment_id else None,
                        'contract_id': payout.contract_id.id if payout.contract_id else None,
                        'contract_name': payout.contract_name,
                        'project_id': payout.project_id.id if payout.project_id else None,
                        'department_id': payout.department_id.id if payout.department_id else None,
                        'partner_id': payout.partner_id.id if payout.partner_id else None,
                        'transaction_value': payout.transaction_value,
                        'amount_with_tax': line.amount,
                        'mnt_amount': line.amount * rate,
                        'currency_id': payout.currency_id.id if payout.currency_id else None,
                        'company_currency_id': payout.company_currency_id.id if payout.company_currency_id else None,
                        'expense_category': payout.expense_category.id if payout.expense_category else None,
                        'payout_company_account': payout.payout_company_account.id if payout.payout_company_account else None,
                        'partner_account': payout.partner_account.id if payout.partner_account else None,
                        'cashflow_type': payout.cashflow_type.id if payout.cashflow_type else None,
                        'pay_date': line.pay_date,
                        'reason': line.reason,
                        'create_date': payout.create_date,
                        'state': payout.state,
                        'attachment_docs':[(6,0,payout.attachment_docs.ids)] if payout.attachment_docs else None
                       # 'payment_docs': [(6,0,[payout.payment_docs])] if payout.payment_docs else None
                        }
                    payout_id = self.env['payout.list'].create(val)
            return True

class split_payment_line(models.TransientModel):
    _name = "split.payment.line"
    _description = "Split Payment Line"
    
    amount = fields.Float('Amount')
    pay_date = fields.Date(u'Төлөх огноо')
    reason = fields.Char(u'Шалтгаан')
    wizard_id = fields.Many2one('split.payment', 'Split Wizard')
