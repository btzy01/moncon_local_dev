# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from dateutil.relativedelta import relativedelta
import time
from datetime import datetime
from datetime import date
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import math
from odoo.http import request
from _sqlite3 import Statement

class PayoutListDocument(models.Model):
	_name = 'payout.document'

	document_id = fields.Many2one('payment.document','Document name')
	file_name = fields.Char('File Name')
	doc_file = fields.Binary('File')

class PayoutList(models.Model):
	_name = 'payout.list'
	_description = 'Payout List'
	_inherit = ['mail.thread']

	@api.multi
	def default_datetime_now(self):
		now = datetime.now()
		return now

	@api.multi
	def _compute_day_diff(self):
		for obj in self:
			from_dt = fields.Datetime.from_string(obj.pay_date)
			to_dt = fields.Datetime.from_string(obj.paid_date)
			if from_dt and to_dt:
				time_delta = to_dt - from_dt
				obj.over_day = math.ceil(time_delta.days + float(time_delta.seconds) / 86400)
			else:
				obj.over_day = 0
	
	@api.depends('create_date')
	def _compute_create_date_txt(self):
		for obj in self:
			obj.create_date_txt = str(obj.create_date)[:10]
				
	@api.depends('pay_date')
	def _compute_pay_date_txt(self):
		for obj in self:
			obj.pay_date_txt = str(obj.pay_date)[:10]
				
	@api.depends('paid_date')
	def _compute_paid_date_txt(self):
		for obj in self:
			obj.paid_date_txt = str(obj.paid_date)[:10]
			
	def _compute_action_date_txt(self):
		for obj in self:
			obj.action_date_txt = str(obj.action_date)

	@api.depends('currency_rate')
	def _compute_mnt_amount(self):
		for obj in self:
			obj.mnt_amount = obj.currency_rate * obj.amount_with_tax

	@api.depends('currency_id')
	def _compute_currency_rate(self):
		for obj in self:
			currency = self.env['res.currency.rate'].search([('currency_id', '=', obj.currency_id.id),('name', '<=', time.strftime("%Y-%m-%d"))], limit=1)
			obj.currency_rate = currency.alter_rate

	@api.depends('currency_id')
	def _compute_currency_rate_date(self):
		for obj in self:
			currency = self.env['res.currency.rate'].search([('currency_id', '=', obj.currency_id.id),('name', '<=', time.strftime("%Y-%m-%d"))], limit=1)
			obj.currency_rate_date = currency.name

	name = fields.Char('List number')

	state = fields.Selection([('expected','Expected'),
							  ('operation_director','ҮА хариуцсан захирал'),
							  ('sent_to_director','Гүйцэтгэх захирал'),
							  ('accepted','Мөнгөн хөрөнгийн нябо'),
							  ('paid','Paid')], readonly=True, default='expected', copy=False, string="State", track_visibility='onchange')
	contract_id = fields.Many2one('contract.management', 'Contract')
	payment_id = fields.Many2one('payment.certificate','Payment number', required=True)
	contract_name = fields.Char('Contract name', readonly=True)
	project_id = fields.Many2one('project.project','Project name', readonly=True)
	project_code = fields.Char(related="project_id.concise_name", readonly=True, default=False, store=True)
	
	department_id = fields.Many2one('hr.department','Department name', readonly=True)
	partner_id = fields.Many2one('res.partner','Contractor name', readonly=True)
	transaction_value = fields.Char('Transaction value')
	amount_with_tax = fields.Float('Give amount', readonly=True)
	currency_id = fields.Many2one('res.currency','Money unit',  readonly=True)
	company_currency_id = fields.Many2one('res.currency', 'Company Currency', default=lambda self: self.env.user.company_id.currency_id)
	currency_rate_date = fields.Date('Currency Rate Date', readonly=True, compute=_compute_currency_rate_date)
	currency_rate_date_paid = fields.Date('Currency Rate Date Paid', readonly=True)
	currency_rate = fields.Float('Currency Rate', readonly=True, compute=_compute_currency_rate)
	currency_rate_paid = fields.Float('Currency Rate Paid', readonly=True)
	mnt_amount = fields.Float('MNT amount', readonly=True, compute=_compute_mnt_amount)
	mnt_amount_paid = fields.Float('Paid MNT amount', readonly=True)
	expense_category = fields.Many2one('account.analytic.account','Expense Category', domain="[('type','=','normal')]")
	cashflow_type = fields.Many2one('account.cashflow.type','Cashflow type', domain="[('type','=','normal')]")

	create_date = fields.Datetime('Create date', required=True, default=default_datetime_now)
	create_date_txt = fields.Char('Create date', compute=_compute_create_date_txt, store=True)
									
	pay_date = fields.Datetime('Pay date')
	pay_date_txt = fields.Char('Pay date', compute=_compute_pay_date_txt, store=True)
	
	paid_date = fields.Datetime('Paid date')
	paid_date_txt = fields.Char('Paid date', compute=_compute_paid_date_txt, store=True)
	
	action_date_txt = fields.Char('Action date')
	
	over_day = fields.Float('Over day', compute=_compute_day_diff)
	
	payout_company_account = fields.Many2one('account.journal','Company account')
	partner_account = fields.Many2one('res.partner.bank','Partner account', domain="[('partner_id','=',partner_id)]")
	company_id = fields.Many2one('res.company','Company', default=lambda self: self.env.user.company_id)
	statement_id = fields.Many2one('account.bank.statement','Bank Statement')
	is_expense_distributing = fields.Boolean('Is expense distributing')
	reason = fields.Char(u'Шалтгаан')

	def action_to_send(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
				'number': payment.name,
				'project': payment.project_id.name if payment.project_id else payment.department_id.name,
				'partner': payment.partner_id.name,
				'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
				'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'l10n_mn_payout_list_action')[1],
				'id': payment.id,
				'db_name': request.session.db,
				'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payout_list_confirm_email_template')
			execute_director_group = model_obj.get_object_reference('l10n_mn_moncon_contract', 'group_first_deputy_director')
			res_user = self.env['res.users'].search([('groups_id', 'in', execute_director_group[1])])
			if res_user:
				user_emails = []
				for user in res_user:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
							'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post(body=email)
			else:
				raise ValidationError(_('Гүйцэтгэх захирал дүр дээр хэрэглэгч байхгүй байна. Админтай холбогдоно уу.'))
			payment.state = 'operation_director'

	@api.onchange('payment_id')
	def onchange_payment_id(self):
		self.project_id = self.payment_id.project_id
		self.department_id = self.payment_id.department_id
		self.partner_id = self.payment_id.partner_id
		self.transaction_value = self.payment_id.note
		self.amount_with_tax = self.payment_id.amount_with_tax
		self.currency_id = self.payment_id.currency_id
		self.expense_category = self.payment_id.expense_category

	@api.multi
	def action_to_send_director(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'l10n_mn_payout_list_action')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payout_list_confirm_email_template')
			execute_director_group = model_obj.get_object_reference('l10n_mn_moncon_contract', 'group_execute_director')
			res_user = self.env['res.users'].search([('groups_id','in', execute_director_group[1])])
			if res_user:
				user_emails = []
				for user in res_user:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True) 
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post(body=email)
			else:
				raise ValidationError(_('Хэрэглэгч байхгүй байна. Админтай холбогдоно уу.'))
			
			return self.write({'state': 'sent_to_director', 'action_date_txt': time.strftime("%Y-%m-%d")})

	@api.multi
	def action_to_accept(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'l10n_mn_payout_list_action')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
	
			template_id = self.env.ref('l10n_mn_moncon_tzh.payout_list_confirm_email_template')
			if payment.project_id:
				if payment.project_id.booker_ids:
					user_emails = []
					for user in payment.project_id.booker_ids:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True) 
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post(body=email)
				else:
					raise ValidationError(_('Төсөл дээр нягтлан бодогчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))
	
			elif payment.department_id:
				if payment.department_id.booker_ids:
					user_emails = []
					for user in payment.department_id.booker_ids:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True) 
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post(body=email)
				else:
					raise ValidationError(_('Албан дээр нягтлан бодогчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))
	
			return self.write({'state': 'accepted', 'action_date_txt': time.strftime("%Y-%m-%d")})


	def _get_period(self):
		periods = self.env['account.period'].find()
		if periods:
			return periods[0]
		return False
	
	@api.one
	def action_to_paid(self):
		for obj in self:
			if not obj.payout_company_account:
				raise UserError(_('Please select company journal.'))
			acc_id = None
			account_id = obj.env['account.account'].search([('user_type_id.type','=','expense')], limit=1)
			
			if account_id:
				acc_id = account_id.id
			else:
				raise ValidationError(_('Зардлын данс олдсонгүй.'))
			
			cost_distribute = []
			if obj.cost_distribution:
				cost_distribute = [dist.id for dist in obj.cost_distribution]
			elif obj.expense_category:
				dist_id = self.env['cost.distribute.line'].create({'cost_category': obj.expense_category.id,
																'transaction_value': obj.name,
																'contractor_amount': obj.currency_rate * obj.amount_with_tax })
				cost_distribute = [dist_id.id]
			
			bank_statement = self.env['account.bank.statement'].search([('journal_id','=',obj.payout_company_account.id),
																		('state','=','open'),
																		('date','=', obj.paid_date if obj.paid_date else time.strftime("%Y-%m-%d"))], limit=1)
			if bank_statement:
				vals = {
					'partner_id': obj.partner_id.id,
					'account_id': acc_id if acc_id else None,
					'statement_id': bank_statement.id,
					'journal_id': obj.payout_company_account.id,
					'currency_id': obj.currency_id.id if obj.payout_company_account.currency_id else None, #Хэрэв төлбөр төлж байгаа журналын валют төгрөг бол валют тавихгүй
					'currency_rate_paid': obj.currency_rate if obj.payout_company_account.currency_id else 1, #Хэрэв төлбөр төлж байгаа журналын валют төгрөг бол 1 гэсэн ханш өгнө
					'currency_rate_date_paid': obj.currency_rate_date,
					'amount': obj.amount_with_tax * (-1) if obj.payout_company_account.currency_id else obj.currency_rate * obj.amount_with_tax * (-1),
					'date': obj.paid_date if obj.paid_date else time.strftime("%Y-%m-%d"),
					'name': obj.name,
					'cashflow_id': obj.cashflow_type.id,
					'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None,
					}
				self.env['account.bank.statement.line'].create(vals)
			else:
				statement_id = self.env['account.bank.statement'].search([('journal_id','=',obj.payout_company_account.id),('state','=','confirm')], limit=1, order='date desc')
				balance_start = 0
				if statement_id:
					balance_start = statement_id[0].balance_end
				vals = {
					'date': obj.paid_date if obj.paid_date else time.strftime("%Y-%m-%d"),
					'journal_id': obj.payout_company_account.id,
					'balance_start': balance_start,
					'state': 'open',
					'currency': obj.currency_id.id,
					}
				bank_statement = self.env['account.bank.statement'].create(vals)
				
				vals = {
					'partner_id': obj.partner_id.id,
					'account_id': acc_id if acc_id else None,
					'statement_id': bank_statement.id,
					'journal_id': obj.payout_company_account.id,
					'currency_id': obj.currency_id.id if obj.payout_company_account.currency_id else None, #Хэрэв төлбөр төлж байгаа журналын валют төгрөг бол валют тавихгүй
					'currency_rate_paid': obj.currency_rate if obj.payout_company_account.currency_id else 1, #Хэрэв төлбөр төлж байгаа журналын валют төгрөг бол 1 гэсэн ханш өгнө
					'amount': obj.amount_with_tax * (-1) if obj.payout_company_account.currency_id else obj.currency_rate * obj.amount_with_tax * (-1),
					'date': obj.paid_date if obj.paid_date else time.strftime("%Y-%m-%d"),
					'name': obj.name,
					'cashflow_id': obj.cashflow_type.id,
					'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None,
					}
				self.env['account.bank.statement.line'].create(vals)
				
			return obj.write({'state': 'paid',
							'paid_date': time.strftime("%Y-%m-%d") if not obj.paid_date else obj.paid_date,
							'mnt_amount_paid': obj.currency_rate * obj.amount_with_tax,
							'currency_rate_paid': obj.currency_rate,
							'currency_rate_date_paid': obj.currency_rate_date,
							'statement_id': bank_statement.id,
							'action_date_txt': time.strftime("%Y-%m-%d")
							})

	@api.one
	def action_to_expected(self):
		return self.write({'state': 'expected'})
	
	@api.one
	def action_to_return(self):
		return self.write({'state': 'expected'})

	@api.one
	def action_to_return_dir(self):
		return self.write({'state': 'operation_director'})

	@api.one
	def action_to_return_accept(self):
		return self.write({'state': 'accepted'})

class IrAttachment(models.Model):
	_inherit = 'ir.attachment'
	payout_id = fields.Many2one('payout.list','Payout list')

class PayoutList(models.Model):
	_inherit = 'payout.list'

	attachment_docs = fields.Many2many('ir.attachment', 'payout_attachment_rel', 'payout_id', 'attachment_id', 'Payout documents')
	payment_docs = fields.Many2many('payment.certificate.document', 'payout_document_rel', 'payout_id', 'doc_id', 'Payout documents')
	cost_distribution = fields.Many2many('cost.distribute.line', 'payout_cost_distribution_rel', 'payout_id', 'cost_id', 'Cost Distribution')
	
	@api.model
	def create(self, vals):
		vals['name'] = self.env['ir.sequence'].next_by_code('payout.list')
		return super(PayoutList, self).create(vals)

	@api.multi
	def unlink(self):
		for payout in self:
			if payout.state not in ('expected'):
				raise ValidationError(_('You cannot delete a payment which is not in expected state.'))
		return super(PayoutList, self).unlink()

class account_journal(models.Model):
    _inherit = 'account.journal'
    is_clearing = fields.Boolean('Is Clearing')
    
class account_move_line(models.Model):
	_inherit = "account.move.line"

	cost_distribution = fields.Many2many('cost.distribute.line', 'move_line_cost_dist_rel', 'line_id', 'cost_id', 'Cost Distribution')

	@api.multi
	def create_analytic_lines(self):
		super(account_move_line, self).create_analytic_lines()
		acc_ana_line_obj = self.env['account.analytic.line']
		for obj_line in self:
			if obj_line.cost_distribution:
				for line2 in obj_line.cost_distribution:
					al_vals={
					   'name': obj_line.name,
					   'date': obj_line.date,
					   'account_id': line2.cost_category.id,
					   'unit_amount': obj_line.quantity,
					   'product_id': obj_line.product_id and obj_line.product_id.id or False,
					   'product_uom_id': obj_line.product_uom_id and obj_line.product_uom_id.id or False,
					   'amount': line2.contractor_amount * (-1) if obj_line.debit > 0 else line2.contractor_amount,
					   'general_account_id': obj_line.account_id.id,
					   'move_id': obj_line.id,
					   'ref': obj_line.ref,
					   'user_id': self._uid,
					   }
					aline = acc_ana_line_obj.create(al_vals)

class account_bank_statement(models.Model):
	_inherit = 'account.bank.statement'
	
# 	@api.one
# 	@api.depends('write_date')
# 	def _compute_remainder(self):
# 		for obj in self:
# 			statement_id = self.env['account.bank.statement'].search([('journal_id','=',obj.journal_id.id)], order='date desc', limit=1)
# 			if obj.id == statement_id.id:
# 				last_statement_id = self.env['account.bank.statement'].search([('journal_id','=',obj.journal_id.id),('is_last','=',True)])
# 				if last_statement_id:
# 					last_statement_id.write({'is_last': False})
# 				obj.is_last = True
	
	is_clearing = fields.Boolean(related="journal_id.is_clearing", readonly=True, store=True)
#	is_last = fields.Boolean('Is Last', store=True, compute='_compute_remainder')

# 	@api.multi
# 	def unlink(self):
# 		for statement in self:
# 			journal_id = False
# 			if statement.is_last:
# 				journal_id = statement.journal_id.id
# 			
# 			if journal_id:
# 				statement_id = self.search([('journal_id','=',journal_id)], order='date desc', limit=1)
# 				if statement_id:
# 					statement_id.write({'is_last': True})
# 		return super(account_bank_statement, self).unlink()

	@api.multi
	def write(self, vals):
		if vals.has_key('state'):
			if not vals['state']:
				vals.update({'state':'open'}) 
		return super(account_bank_statement, self).write(vals)
	
class account_bank_statement_line(models.Model):
	_inherit = 'account.bank.statement.line'
	
	cost_distribution = fields.Many2many('cost.distribute.line', 'bank_line_cost_dist_rel', 'bank_line_id', 'cost_id', 'Cost Distribution')
	is_expense_distributing = fields.Boolean('Is expense distributing')

	def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
		res = super(account_bank_statement_line, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec, new_aml_dicts)
		for move in res:
			cost_distribute = []
			if self.cost_distribution:
				cost_distribute = [dist.id for dist in self.cost_distribution]
			for move_line in move.line_ids:
				if not move_line.cashflow_id:
					move_line.write({'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None})
					move_line.create_analytic_lines()
		return res

# 	@api.multi
# 	def button_confirm(self):
# 		move_ids = []
# 		statement_obj = self.env['account.bank.statement']
# 		for st_line in self:
# 			if not st_line.amount:
# 				continue
# 			if st_line.statement_id.journal_id.restrict_negative_bank_statement == True:
# 				if st_line.statement_id.balance_end < 0.0:
# 					raise ValidationError(_('Balance can not be negative'))
# 			if not st_line.account_id:
# 				raise ValidationError(_('There is no Account'))
# 			
# 			cost_distribute = []
# 			if st_line.cost_distribution:
# 				cost_distribute = [dist.id for dist in st_line.cost_distribution]
# 				
# 			if st_line.account_id and not st_line.journal_entry_id.id:
# 				vals = {
# 					'debit': st_line.amount < 0 and -st_line.amount or 0.0,
# 					'credit': st_line.amount > 0 and st_line.amount or 0.0,
# 					'account_id': st_line.account_id.id,
# 					'name': st_line.name,
# 					'counterpart_move_line_id': st_line.import_line_id.id,
# 					'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None,
# 				}
# 				move_id = self.process_reconciliation(st_line.id, [vals])
# 				self.env['account.move'].post(cr, uid, move_id, context=context)
# 			if st_line.statement_id and st_line.statement_id.state == 'confirm':
# 				statement_obj.write(st_line.statement_id.id, {'balance_end_real': st_line.statement_id.balance_end or 0.0})
# 				# тухайн ордероос хойшхи хаагдсан ордер байвал үлдэгдлүүдийг шинэчлэнэ.
# 				after_state_ids = statement_obj.search([('id','<>',st_line.statement_id.id),
# 									  ('state','in',['open','draft','confirm']),
# 									  ('journal_id','=',st_line.statement_id.journal_id.id),
# 									  ('date','>=',st_line.statement_id.date),
# 									  ('date','>=',st_line.statement_id.period_id.fiscalyear_id.date_start)], order='date')
# 				# Pos-г accountant manager-с бага эрхтэй хүн хаах боломжтой болох
# 				pos = False
# 				if self.env['pos.order']:
# 					pos = st.pos_session_id
# 				if not pos:		
# 					if after_state_ids and not self.env['res.users'].has_group('account.group_account_user'):
# 						state_date = self.read(after_state_ids[0], ['date'])['date']
# 						raise ValidationError(_('You cannot open cash statement on %s date. Because this cash order already opened on %s date. This action only allowed for Accountant Manager.') %(st.date, state_date))
# 					
# 				# Тухайн журнал дээрх тухайн ордероос хойшхи хаагдсан ордерууд байвал эхний болон эцсийн үлдэгдлийг шинэчилж тааруулна.  
# 				if after_state_ids:
# 					balance_start = st_line.statement_id.balance_end or 0.0# Нээлтийн үлдэгдэл
# 					statement_obj.resolve_statement_balance(after_state_ids, balance_start)
# 			if st_line.related_move_id:
# 				self.env['account.move'].post(st_line.related_move_id.id)
# 			if st_line.related_statement_line_id and not st_line.related_statement_line_id.journal_entry_id:
# 				self.button_confirm([st_line.related_statement_line_id.id])
# 		return self.write({'state':'confirm'})	