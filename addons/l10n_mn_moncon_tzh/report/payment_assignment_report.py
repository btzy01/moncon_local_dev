# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import api, models, _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me ,convert_curr
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import time

class PaymentAssignmentReportView(models.AbstractModel):
	_name = 'report.l10n_mn_moncon_tzh.payment_assignment_report_view'

	@api.model
	def render_html(self, docids, data=None):

		seq = self.env['ir.sequence'].next_by_code('report.l10n_mn_moncon_tzh.payment_assignment_report_view')
		print '\n\nseq', seq

		# ctx = self._context['active_ids'].copy()
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('l10n_mn_moncon_tzh.payment_assignment_report_view')
		statement_line_obj = self.env['payout.list']
		
		lines = statement_line_obj.browse(docids)

		verbose_total_dict = {}
		amounts = {}
		currency = {}
		company_account_name = {}
		company_account = {}
		current_date = {}
		name = {}

		for line in lines:
			word = verbose_numeric(abs(line.amount_with_tax))
			curr = u''
			div_curr = u''
			symbol = u''
			if line.currency_id:
				curr = line.currency_id.integer
				div_curr = line.currency_id.divisible
				symbol = line.currency_id.symbol
			
			verbose_total_dict[line.id] = convert_curr(word, curr, div_curr)
			
			bank_id = self.env['res.partner.bank'].search([('journal_id','=',line.payout_company_account.id)])
			if bank_id:
				company_account_name[line.id] = bank_id[0].bank_name
				company_account[line.id] = bank_id[0].acc_number
			else:
				raise UserError(_('This journal is not defined in company configuration!!!'))
				
			amounts[line.id] = comma_me(abs(line.amount_with_tax))

			currency[line.id] = {'name': curr,
								 'symbol': symbol}
			current_date[line.id] = time.strftime("%Y-%m-%d")

			name[line.id] = line.payout_company_account.code + time.strftime("%Y%m%d")

		docargs = {
			'doc_ids': docids,
			'doc_model': report.model,
			'docs': lines,
			'verbose_amount': verbose_total_dict,
			'company_account_name': company_account_name,
			'company_account': company_account,
			'current_date': current_date,
			'amounts': amounts,
			'currency': currency,
			'name': name,
			'data_report_margin_top': 30,
            'data_report_header_spacing': 5
			}
		
		return report_obj.render('l10n_mn_moncon_tzh.payment_assignment_report_view', docargs)