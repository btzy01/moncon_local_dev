# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import api, models, _
from odoo.exceptions import UserError, ValidationError
import traceback
import base64
from subprocess import Popen, PIPE
from pyPdf import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
from StringIO import StringIO
import os, errno
from docx import Document
from PIL import Image
import tempfile
from odoo.addons.l10n_mn_report.models.report_helper import comma_me
import time

class PaymentCertificateReportView(models.AbstractModel):
	_name = 'report.l10n_mn_moncon_tzh.payment_certificate_report_view'

	@api.model
	def render_html(self, docids, dst=None, data=None):
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('l10n_mn_moncon_tzh.payment_certificate_report_view')
		statement_line_obj = self.env['payment.certificate']
		lines = statement_line_obj.browse(docids)
		
		company_account_name = {}
		company_account = {}
		before_deduction = {}
		report_deduction = {}
		total_deduction = {}
		no_vat_report = {}
		no_vat_before = {}
		no_vat_total = {}
		contract_number = {}
		payment = {}


		for line in lines:
			#if line.allow_payment_types == '1':
				#raise ValidationError(_('You cant print contract payment certificate'))
			
			before = 0
			reports = 0
			total = 0
			no_tax_deduc = 0
			no_tax_deduc_before = 0
			bank_acc = self.env['res.partner.bank'].search([('journal_id','=',line.company_bank_id.id)], limit=1)
			if bank_acc:
				company_account_name[line.id] = bank_acc.bank_id.name
				company_account[line.id] = bank_acc.acc_number
# 			else:
# 				raise UserError(_('This journal is not defined in company configuration.'))

			for ded in line.deductions_line_ids:
				before = before + ded.deductions_before
			before_deduction[line.id] = comma_me(abs(before))

			for ded_report in line.deductions_line_ids:
				reports = reports + ded_report.deductions_report
			report_deduction[line.id] = comma_me(abs(reports))

			for ded_total in line.deductions_line_ids:
				total = total + ded_total.deductions_total
			total_deduction[line.id] = comma_me(abs(total))
			
			payment_information_line = {}
			payment_line_without_vat = {}
			deductions_no_tax_lines = {}
			deductions_lines = {}
			
			current_amount = 0
			before_amount = 0
			for pay_info in line.payment_information_line_ids:
				for deduc_no_tax in line.deductions_no_tax_line_ids:
					no_tax_deduc = no_tax_deduc + deduc_no_tax.deductions_report
					no_tax_deduc_before = no_tax_deduc_before + deduc_no_tax.deductions_before
					
				payment_information_line[pay_info.id] = {'before_amount': comma_me(abs(pay_info.before_amount)),
														'current_amount': comma_me(abs(pay_info.current_amount)),
														'total_amount': comma_me(abs(pay_info.total_amount))}
				current_amount += pay_info.current_amount
				before_amount += pay_info.before_amount
				
				
				payment_line_without_vat[pay_info.id] = {'no_vat_before': comma_me(abs(before_amount - no_tax_deduc_before)),
														'no_vat_report': comma_me(abs(current_amount - no_tax_deduc)),
														'no_vat_total': comma_me(abs(current_amount - no_tax_deduc + before_amount - no_tax_deduc_before))}
				
			contract_number[line.id] = line.contract_id.old_name if line.contract_id.old_name else line.contract_id.names
			payment[line.id] = {'amount_without_tax': comma_me(abs(line.amount_without_tax)),
							'additional_amount': comma_me(abs(line.additional_amount)),
							'total_amount': comma_me(abs(line.total_amount)),
							'over_budget_amount': comma_me(abs(line.over_budget_amount)),
							'payment_amount': comma_me(abs(line.payment_amount)),
							'tax_amount': comma_me(abs(line.tax_amount)),
							'year': time.strftime("%Y"),
							'month': time.strftime("%m"),
							'day': time.strftime("%d"),
							}
			for ded in line.deductions_no_tax_line_ids:
				deductions_no_tax_lines[ded.id] = {'ded_name': ded.deductions_ids.name,
													'deductions_before': comma_me(abs(ded.deductions_before)),
													'deductions_report': comma_me(abs(ded.deductions_report)),
													'deductions_total': comma_me(abs(ded.deductions_total)),
												}
			
			for ded in line.deductions_line_ids:
				deductions_lines[ded.id] = {'ded_name': ded.deductions_ids.name,
											'deductions_before': comma_me(abs(ded.deductions_before)),
											'deductions_report': comma_me(abs(ded.deductions_report)),
											'deductions_total': comma_me(abs(ded.deductions_total)),
											}

		docargs = {
			'doc_ids': docids,
			'doc_model': report.model,
			'docs': lines,
			'company_account_name': company_account_name,
			'company_account': company_account,
			'before_deduction': before_deduction,
			'report_deduction': report_deduction,
			'total_deduction': total_deduction,
			'payment_information_line': payment_information_line,
			'payment_line_without_vat': payment_line_without_vat,
			'no_vat_report': no_vat_report,
			'no_vat_before': no_vat_before,
			'no_vat_total': no_vat_total,
			'contract_number': contract_number,
			'payment': payment,
			'deductions_no_tax_lines': deductions_no_tax_lines,
			'deductions_lines': deductions_lines
			}
	
		return report_obj.render('l10n_mn_moncon_tzh.payment_certificate_report_view', docargs)
