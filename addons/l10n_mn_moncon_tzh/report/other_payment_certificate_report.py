# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import api, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons.l10n_mn_report.models.report_helper import comma_me
import time

class PaymentCertificateReportView(models.AbstractModel):
	_name = 'report.l10n_mn_moncon_tzh.other_payment_certificate_report_view'

	@api.model
	def render_html(self, docids, data=None):
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('l10n_mn_moncon_tzh.other_payment_certificate_report_view')
		statement_line_obj = self.env['payment.certificate']
		lines = statement_line_obj.browse(docids)

		company_account_name = {}
		company_account = {}
		amount = {}
		payment = {}
		documents = []

		
		for line in lines:
			if line.allow_payment_types != '2':
				raise ValidationError(_('You cant print other payment certificate'))
			#bank_id = self.env['res.partner.bank'].search([('journal_id','=',line.company_bank_id.id)])
			bank_acc = self.env['res.partner.bank'].search([('journal_id','=',line.company_bank_id.id)], limit=1)
			if bank_acc:
				company_account_name[line.id] = bank_acc.bank_id.name
				company_account[line.id] = bank_acc.acc_number
			else:
				raise UserError(_('This journal is not defined in company configuration!!'))
			for doc in line.attach_docs:
				documents.append(doc)
				
			amount[line.id] = comma_me(abs(line.currency_amount))
			
			payment[line.id] = {'over_budget_amount': comma_me(abs(line.over_budget_amount)),
							'year': time.strftime("%Y"),
							'month': time.strftime("%m"),
							'day': time.strftime("%d"),
							}
			

		docargs = {
			'doc_ids': docids,
			'doc_model': report.model,
			'docs': lines,
			'company_account_name': company_account_name,
			'company_account': company_account,
			'amount': amount,
			'payment': payment,
			'documents': documents,
			}
	
		return report_obj.render('l10n_mn_moncon_tzh.other_payment_certificate_report_view', docargs)