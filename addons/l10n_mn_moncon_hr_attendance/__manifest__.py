# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Moncon Hr Attendance",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr_attendance', 'l10n_mn_hr_attendance_hour_balance', 'l10n_mn_hr_attendance_repair', 'web_readonly_bypass'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         Moncon Hr Attendance
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'wizard/cancel_comment_view.xml',
        'views/moncon_hr_attendance_view.xml',
        'wizard/hr_attendance_calculation.xml',
       'views/hr_attendance_repair_view.xml',
        'views/hr_attendance_detail_view.xml',
        'wizard/hr_attendance_repair_view.xml'
        # 'views/hr_attendance_repair_line.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
