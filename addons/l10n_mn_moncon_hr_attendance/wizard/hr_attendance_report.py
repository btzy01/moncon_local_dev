# -*- coding: utf-8 -*-

import base64
from io import BytesIO
from lxml import etree
import time
import xlsxwriter

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.models.report_helper import *
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import AccessError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class HRAttendanceReport(models.TransientModel):
    """
        ИРЦИЙН ДЭЛГЭРЭНГҮЙ ТАЙЛАН
    """
    _inherit = 'hr.attendance.report'

    def get_holiday_type(self, type):
        if type == 'paid':
            type = _('p/hol')
        elif type == 'unpaid':
            type = _('u.p/hol')
        elif type == 'annual_leave':
            type = _('a.l/hol')
        elif type == 'sick_leave':
            type = _('s.l/hol')
        elif type == 'paid_holiday':
            type = _('Н.А/чөл')
        return type

    def get_value(self, sheet, rowx, content_formats):
        date_end = datetime.strptime(self.date_to, DEFAULT_SERVER_DATE_FORMAT)
        sequence = 1
        weekend_day = _('we/day')
        absent = _('Abs')
        uncompleted = _('half/att')
        out_work = _('out/work')

        for employee in self.employee_ids:
            col_att = 6
            date_start = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT)
            identities = employee.mapped('attendance_device_ids').mapped('employee_device_id') or []

            sheet.write(rowx, 0, sequence, content_formats['format_content_center'])
            sheet.write(rowx, 1, ", ".join(str(identity) for identity in identities) or "", content_formats['format_content_center'])
            sheet.write(rowx, 2, employee.last_name or "", content_formats['format_content_text'])
            sheet.write(rowx, 3, employee.name_related, content_formats['format_content_text'])
            sheet.write(rowx, 4, employee.job_id.name or "", content_formats['format_content_text'])
            sheet.write(rowx, 5, employee.department_id.name or "", content_formats['format_content_text'])

            while date_start <= date_end:
                is_weekend = self.get_day_is_weekend(date_start)
                date_from = str(get_display_day_to_user_day(date_start.strftime(DEFAULT_SERVER_DATE_FORMAT) + ' 00:00:00', self.env.user))
                date_to = str(get_display_day_to_user_day(date_start.strftime(DEFAULT_SERVER_DATE_FORMAT) + ' 23:59:59', self.env.user))

                domain = [('employee_id', '=', employee.id)]
                domain.extend(get_duplicated_day_domain('check_in', 'check_out', date_from, date_to, self.env.user))
                att_id = self.env['hr.attendance'].search(domain, limit=1)

                balance_types, holiday_status = False, ""
                if self.env.get('hr.holidays', False) != False:
                    domain = [('employee_id', '=', employee.id), ('state', '=', 'validate'), ('date_from', '!=', False), ('date_to', '!=', False)]
                    domain.extend(get_duplicated_day_domain('date_from', 'date_to', date_from, date_to, self.env.user))
                    balance_types = list(set(self.env['hr.holidays'].search(domain).mapped('holiday_status_id')))
                    print balance_types, 'aaaa\n\n'
                    for balance_type in balance_types:
                        if balance_type.is_nohon_amrakh:
                            holiday_status = ('%s' % self.get_holiday_type('paid_holiday'))
                        else:
                            holiday_status = ('%s' % self.get_holiday_type(balance_type.balance_type))
                event_ids = False
                if self.env.get('calendar.event', False) != False and hasattr(self.env.get('calendar.event'), 'is_outside_work'):
                    partner_id = employee.address_home_id or (employee.user_id.sudo().partner_id if employee.user_id and employee.user_id.sudo().partner_id else False)
                    if partner_id:
                        domain = [('company_id', '=', employee.company_id.id), ('is_outside_work', '=', True), ('partner_ids', 'in', partner_id.id)]
                        domain.extend(get_duplicated_day_domain('start', 'stop', date_from, date_to, self.env.user))
                        if hasattr(employee.company_id, 'require_cnfrmtn_on_outwork') and employee.company_id.require_cnfrmtn_on_outwork:
                            domain.append(('state', '=', 'open'))
                        event_ids = self.env['calendar.event'].search(domain)

                value, format = '', content_formats['format_content_center']
                if not att_id:
                    if balance_types:
                        value, format = '%s' % holiday_status, content_formats['format_blue_text']
                    if event_ids:
                        value += ('%s' % out_work if not value else '\n%s' % out_work)
                        format = content_formats['format_blue_text']
                    if is_weekend:
                        value += ('%s' % weekend_day if not value else '\n%s' % weekend_day)
                        format = content_formats['format_black_text_greyed_out']
                    if not (is_weekend or balance_types or event_ids):
                        value, format = '%s' % absent, content_formats['format_red_text']
                else:
                    check_in_hour = get_day_like_display(att_id.check_in, self.env.user).strftime('%H:%M') if att_id.check_in else False
                    check_out_hour = get_day_like_display(att_id.check_out, self.env.user).strftime('%H:%M') if att_id.check_out else False

                    if check_in_hour and check_out_hour:
                        if not att_id.is_attendance_repair:
                            value = "%s\n%s" % (check_in_hour, check_out_hour)
                            format = content_formats['format_black_text']
                            if balance_types:
                                value += '\n%s' % holiday_status
                                format = content_formats['format_blue_text']
                            if event_ids:
                                value += '\n%s' % out_work
                                format = content_formats['format_blue_text']
                            if is_weekend:
                                value += '\n%s' % weekend_day
                                format = content_formats['format_black_text_greyed_out']
                        else:
                            value = "%s\n%s" % (check_in_hour, check_out_hour)
                            format = content_formats['format_green_text']
                            if balance_types:
                                value += '\n%s' % holiday_status
                                format = content_formats['format_green_text']
                            if event_ids:
                                value += '\n%s' % out_work
                                format = content_formats['format_green_text']
                            if is_weekend:
                                value += '\n%s' % weekend_day
                                format = content_formats['format_black_text_greyed_out']
                    else:
                        value = "%s" % (check_in_hour or check_out_hour)
                        format = content_formats['format_red_text']
                        if balance_types:
                            value += '\n%s' % holiday_status
                        if event_ids:
                            value += '\n%s' % out_work
                        value += "\n%s" % uncompleted

                sheet.write(rowx, col_att, value, format)

                col_att +=1
                date_start += timedelta(days=1)

            sequence += 1
            sheet.set_row(rowx, 30)
            rowx +=1

        return sheet, rowx

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('HR Attendance Detail Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_filter = ReportExcelCellStyles.format_filter
        format_group = ReportExcelCellStyles.format_content_header
        format_content_center = ReportExcelCellStyles.format_content_center
        format_content_text = ReportExcelCellStyles.format_content_text
        format_red_text = ReportExcelCellStyles.format_content_center_red_text
        format_blue_text = ReportExcelCellStyles.format_content_center_blue_text
        format_green_text = ReportExcelCellStyles.format_content_center_green_text
        format_black_text = ReportExcelCellStyles.format_content_center_float

        # for zoom
        format_filter['font_size'] = 14
        format_group['font_size'] = 14
        format_content_center['font_size'] = 14
        format_content_text['font_size'] = 14
        format_red_text['font_size'] = 14
        format_blue_text['font_size'] = 14
        format_green_text['font_size'] = 14
        format_black_text['font_size'] = 14
        format_green_text['font_color'] = '#00a65d'

        format_black_text_greyed_out = format_black_text.copy()
        format_green_text_greyed_out = format_green_text.copy()
        format_black_text_greyed_out['bg_color'] = '#dddddd'
        format_green_text_greyed_out['bg_color'] = '#dddddd'
        format_green_text_greyed_out['font_color'] = '#037751'

        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(format_filter)
        format_group = book.add_format(format_group)
        format_content_center = book.add_format(format_content_center)
        format_content_text = book.add_format(format_content_text)
        format_red_text = book.add_format(format_red_text)
        format_blue_text = book.add_format(format_blue_text)
        format_green_text = book.add_format(format_green_text)
        format_black_text = book.add_format(format_black_text)
        format_black_text_greyed_out = book.add_format(format_black_text_greyed_out)
        format_green_text_greyed_out = book.add_format(format_green_text_greyed_out)

        header_formats = {
            'format_filter': format_filter,
            'format_name': format_name,
        }

        content_formats = {
            'format_group': format_group,
            'format_content_center': format_content_center,
            'format_content_text': format_content_text,
            'format_red_text': format_red_text,
            'format_blue_text': format_blue_text,
            'format_green_text': format_green_text,
            'format_black_text': format_black_text,
            'format_black_text_greyed_out': format_black_text_greyed_out,
            'format_green_text_greyed_out': format_green_text_greyed_out,
        }

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('hr_attendance_detail_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet = self.get_sheet_format(sheet)

        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:D', 13)
        sheet.set_column('E:F', 20)

        # create header
        rowx = 0
        sheet, rowx = self.get_header(sheet, header_formats, rowx, report_name)

        # create content
        sheet, rowx = self.get_content_value_header(sheet, rowx, content_formats)
        sheet, rowx = self.get_value(sheet, rowx, content_formats)

        # close workbook and export report
        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()
