# -*- coding: utf-8 -*-

import base64
from datetime import datetime, timedelta
import decimal
import logging
import pytz
import time

import xlrd

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


_logger = logging.getLogger(__file__)


class HrAttendanceCalculate(models.TransientModel):
    _name = "hr.attendance.calculate"
    
    date_from = fields.Date('Date From', required=True)
    date_to = fields.Date('Date To', required=True)
    department_ids = fields.Many2many('hr.department', 'hr_attendance_calc_department_rel', 'wizard_id', 'department_id', 'Departments', ondelete='cascade')
    employee_ids = fields.Many2many('hr.employee', 'hr_attendance_calc_employee_rel', 'wizard_id', 'employee_id', 'Employees', ondelete='cascade')
    
    def calculate_raw_attendance(self):
        self.ensure_one()
        _logger.info('========================================')
        _logger.info(_(u'Started calculating raw attendance.'))

        date_from = datetime.strptime('%s 00:00:00' % str(self.date_from), DEFAULT_SERVER_DATETIME_FORMAT)
        date_to = datetime.strptime('%s 23:59:59' % str(self.date_to), DEFAULT_SERVER_DATETIME_FORMAT)
        
        employee_ids = self.employee_ids if self.employee_ids else self.env['hr.employee'].search([])
        for employee in employee_ids:
            self.calculate_employee_attendances(employee, date_from, date_to)
            
    def get_device_name1(self, date, attendances, device_name=False):
        for att in attendances:
            if str(att.date) == str(date):
                return att.device
        return device_name
          
    def get_available_min_max_date(self, from_date, to_date, dates):
                
        min_date, max_date = False, False
        from_date = datetime.strptime(str(from_date), DEFAULT_SERVER_DATETIME_FORMAT)
        to_date = datetime.strptime(str(to_date), DEFAULT_SERVER_DATETIME_FORMAT)
        available_dates = []
        
        for date in dates:
            date = datetime.strptime(str(date), DEFAULT_SERVER_DATETIME_FORMAT)
            if from_date <= date and date <= to_date:
                available_dates.append(date)
            else:
                available_dates.append(False)
        
        if available_dates and len(available_dates) > 0:
            
            for available_date in available_dates:
                if available_date:
                    min_date, max_date = available_date, available_date
                    break
                    
            for available_date in available_dates:
                if available_date and available_date < min_date:
                    min_date = available_date
                if available_date and available_date > max_date:
                    max_date = available_date
                     
        return str(min_date) if min_date else False, str(max_date) if max_date else False
  
    @api.multi
    def calculate_employee_attendances(self, employee, date_from_att=False, date_to_att=False):
        user_time_zone = pytz.timezone(self.env.user.tz)
        utc = pytz.UTC

        date_from = date_from_att
        date_to = date_to_att
        
        if not date_from:
            date_from = datetime.strptime('%s 00:00:00' % self.date_from, DEFAULT_SERVER_DATETIME_FORMAT)
        if not date_to:
            date_to = datetime.strptime('%s 23:59:59' % self.date_to, DEFAULT_SERVER_DATETIME_FORMAT)
            
        date_from = str(get_day_by_user_timezone(date_from, self.env.user))
        date_to = str(get_day_by_user_timezone(date_to, self.env.user))
                
        date = None
        day_attendance = self.env['hr.attendance']
        raw_data = self.env['hr.attendance.raw.data'].search([('employee_id', '=', employee.sudo().id), ('date', '>=', date_from), ('date', '<=', date_to)])
        attendances_days = [get_day_like_display(str(att.date), self.env.user) for att in raw_data]
        calculated_days = []
        
        for att in raw_data:
            day_of_att = str(get_day_like_display(str(att.date), self.env.user))[:10]
            if day_of_att not in calculated_days:
                calculated_days.append(day_of_att)
                check_in, check_out = self.get_available_min_max_date('%s 00:00:00' % day_of_att, '%s 23:59:59' % day_of_att, attendances_days)
                
                if check_in or check_out:
                    if not check_in:
                        check_in = datetime.strptime(check_out, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)
                        
                    if not check_out:
                        check_out = datetime.strptime(check_in, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=-1)
                        
                    day_attendance = self.env['hr.attendance'].create({
                        'employee_id': employee.id,
                        'check_in': str(get_day_to_display_timezone(check_in, self.env.user)),
                        'check_out': str(get_day_to_display_timezone(check_out, self.env.user)),
                        'in_device_name': self.get_device_name1(get_day_to_display_timezone(check_in, self.env.user), raw_data),
                        'out_device_name': self.get_device_name1(get_day_to_display_timezone(check_out, self.env.user), raw_data),
                        'company_id': employee.company_id.id,
    #                         'in_time': datetime.strptime(date, '%H:%M'),
                    })
                    att.write({'attendance_id': day_attendance.id})
