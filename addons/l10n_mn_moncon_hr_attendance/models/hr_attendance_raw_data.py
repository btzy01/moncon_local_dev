# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import AccessError
from lxml import etree

class HrAttendanceRawData(models.Model):
    
    _inherit = "hr.attendance.raw.data"
    
    device_id = fields.Integer('Device id')
    
    @api.model
    def create(self, vals):
        if 'identification_id' in vals.keys() and 'device_id' in vals.keys() and not 'employee_id' in vals.keys():
            employee_id = self.env['hr.employee'].search([('identification_id', '=', vals['identification_id'])])
            if employee_id:
                vals['employee_id'] = employee_id.id
            if not employee_id:
                device_regs = self.env['hr.employee.attendance.device'].search([('employee_device_id', '=', vals['identification_id']), ('device_id', '=', vals['device_id'])], limit=1)
                if device_regs and device_regs[0].employee_id:
                    vals['employee_id'] = device_regs[0].employee_id.id
                    
        return super(HrAttendanceRawData, self).create(vals)