# -*- coding: utf-8 -*-
import logging

from odoo import api, fields, models
from datetime import datetime
import pytz


_logger = logging.getLogger(__name__)


class HrAttendanceDetail(models.Model):
    _name = "hr.attendance.detail"
    _order = 'report_datetime DESC'

    name = fields.Char('Нэр')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')
    holidays_type = fields.Char('Амралт чөлөөний', compute='_holidays_type')
    holidays_date = fields.Datetime(string="Check Out", compute='_compute_check_in')
    report_datetime = fields.Date(u'Өдөр', required=True)
    employee_id = fields.Many2one('hr.employee', u'Ажилтан')

    @api.model
    def create(self, vals):
        if 'report_datetime' in vals:
            date = self.search([('report_datetime', '=', vals['report_datetime'])])
            if date:
                date.unlink()
        return super(HrAttendanceDetail, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'report_datetime' in vals:
            date = self.search([('report_datetime', '=', vals['report_datetime'])])
            if date:
                date.unlink()
        return super(HrAttendanceDetail, self).write(vals)

    @api.multi
    def _compute_check_in(self):
        for obj in self:
            if obj.report_datetime:
                employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
                start = datetime.strptime(obj.report_datetime, "%Y-%m-%d")
                local = pytz.timezone(self.env.user.tz)
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(employee, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.report_datetime = attendance.check_in
                    obj.check_out = attendance.check_out
                if not attendances:
                    date_time_obj = pytz.utc.localize(datetime.strptime(obj.report_datetime, '%Y-%m-%d')).astimezone(local)
                    date = date_time_obj.date()
                    holidays = self.env['hr.holidays'].search(
                        [('employee_id', '=', employee.id), ('date_from', '>=', str(date) + ' 00:00:00'), ('date_from', '<=', str(date) + ' 23:59:59')])
                    for hol in holidays:
                        obj.check_in = hol.date_from
                        obj.check_out = hol.date_to

    @api.multi
    def _holidays_type(self):
        for obj in self:
            local = pytz.timezone(self.env.user.tz)
            employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
            if obj.check_in and employee:
                date_time_obj = pytz.utc.localize(datetime.strptime(obj.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(local)
                date = date_time_obj.date()
                holidays = self.env['hr.holidays'].search(
                    [('employee_id', '=', employee.id), ('date_from', '>=', str(date) + ' 00:00:00'), ('date_from', '<=', str(date) + ' 23:59:59'), ('state', '=', 'validate')])
                holiday = ''
                for hol in holidays:
                    holiday = str(hol.holiday_status_id.name) + str(hol.number_of_hours_temp) + u'цаг'
                rapair_attendances = self.env['hr.attendance.repair.line'].search(
                    [('attendance_repair.employee_id', '=', employee.id), ('date_of_attendance', '>=', str(date) + ' 00:00:00'), ('date_of_attendance', '<=', str(date) + ' 23:59:59'), ('attendance_repair.state', '=', 'approved')])
                repair_attendance = ''
                for repair in rapair_attendances:
                    repair_attendance = str(repair.reason_of_attendance.name)
                obj.holidays_type = repair_attendance + holiday

    def create_attendance_repair(self):
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'hr.attendance.repair',
                'view_id': self.env.ref('l10n_mn_hr_attendance_repair.hr_attendance_repair_form').id,
                'context': {'default_date_of_attendance': obj.report_datetime if obj.report_datetime else False},
                'target': 'new'
            }

    def create_holidays(self):
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'hr.holidays',
                'view_id': self.env.ref('l10n_mn_moncon_hr_holidays.view_holiday_form_inherit_moncon').id,
                'context': {'default_date_from': obj.report_datetime if obj.report_datetime else False, 'default_date_to': obj.report_datetime if obj.report_datetime else False},
                'target': 'new'
            }

    def nohon_amrakh_holidays(self):
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'moncon.hr.holidays',
                'view_id': self.env.ref('l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view').id,
                'context': {'default_date_from': obj.report_datetime if obj.report_datetime else False,
                            'default_date_to': obj.report_datetime if obj.report_datetime else False},
                'target': 'new'
            }
