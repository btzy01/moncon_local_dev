# -*- coding: utf-8 -*-

{
    'name': "Moncon HR - Hour balance",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_hour_balance',
    ],
    'author': "Moncon LLC",
    'website': 'http://moncon.erp.mn',
    'category': 'Moncon Modules',
    'description': """
      Employee Hour balance
    """,
    'data': [
        'security/balance_security.xml',
        'views/hour_balance_view.xml',
        'views/hr_payroll_view.xml',
        'report/print_employee_balance_report.xml',
        'wizard/hour_balance_print_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
