# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models
import logging
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)

class HourBalance(models.Model):
    _inherit = "hour.balance"


    @api.depends('department_id', 'period_id', 'salary_type', 'hr_employee_id')
    def compute_balance_name(self):
        for record in self:
            name = ''
            name_change = False
            if record.hr_employee_id:
                name += _("%s ХН-н ") % record.hr_employee_id.name
                name_change = True
            if record.department_id:
                name += _("%s departments's ") % record.department_id.name
                name_change = True
            if record.period_id:
                name += _("%s month's ") % record.period_id.name
                name_change = True
            if record.salary_type:
                name += _("%s hour balance") % dict(self._fields['salary_type']._description_selection(self.env)).get(record.salary_type)
                name_change = True
            if name_change:
                record.name = name

    hr_employee_id = fields.Many2one('hr.employee', 'ХН ажилтан')
    department_hr_tag_ids = fields.Many2one('department.hr.tag',  string="Tags")


    @api.multi
    def approve(self):
        for hour_balance in self:
            kpi_id = self.env['moncon.kpi'].search([('period_id', '=', hour_balance.period_id.id), ('kpi_state', '=', 'done')], limit=1)
            if hour_balance.salary_type == 'last_salary':
                if len(kpi_id) == 0:
                    raise ValidationError(_('KPI-н үнэлгээ батлагдаж дуусаагүй байна!'))
            for line in hour_balance.balance_line_ids:
                line.write({'state': 'done'})
        self.write({'state': 'done'})


    @api.multi
    def import_all_kpi(self):
        for obj in self:
            for line in obj.balance_line_ids:
                line.action_import_kpi_line()



    # def get_linked_objects(self, object, employee, st_date, end_date):
    #     if object != 'hr.holidays':
    #         return self.env['object'].sudo().search([])
    #
    #     domain = [('employee_id', '=', employee.id), ('type', '=', 'remove'), ('state', '=', 'accepted')]
    #     if 'balance_type' in self._context.keys():
    #         domain.append(('holiday_status_id.balance_type', '=', self._context['balance_type']))
    #         domain.append(('holiday_status_id.is_nohon_amrakh', '=', self._context['is_nohon_amrakh']))
    #     domain.extend(get_duplicated_day_domain('date_from', 'date_to', st_date, end_date, self.env.user))
    #
    #     return self.env[object].sudo().search(domain)

    def get_available_employees(self):
        employee_obj = self.env['hr.employee'].sudo()
        employees = []

        for obj in self:
            # 'Урьдчилгаа цалин' төрөлтэй цагийн баланс үед 'Жирэмсэн' болон 'Ажлаас гарсан' төлөвтэй ажилчдыг НЭМЭХГҮЙ байх
            ignored_states = ['resigned', 'maternity'] if obj.salary_type == 'advance_salary' else ['resigned']
            ignored_statuses = self.env['hr.employee.status'].sudo().search([('type', 'in', ignored_states)])

            # Update of balance sheet information
            domain = [('active', '=', True), ('company_id', '=', obj.company_id.id)]
            if ignored_statuses and len(ignored_statuses) > 0:
                domain.append(('state_id', 'not in', ignored_statuses.sudo().ids))
            if obj.department_id:
                domain.append(('department_id', 'child_of', [obj.department_id.id]))
            if obj.hr_employee_id:
                domain.append(('hr_employee_id', '=', obj.hr_employee_id.id))
            if obj.department_hr_tag_ids:
                 domain.append(('department_hr_tag_ids', '=', obj.department_hr_tag_ids.id))
            if obj.salary_type == 'advance_salary':
                 domain.append(('contract_id.is_only_pay_vat', '=', False))
            employees.extend(employee_obj.search(domain))
        return employees


    def get_hb_lines_by_employee_status(self):
        hour_balance_line_obj = self.env['hour.balance.line']
        existen_emps = []
        for line in self.balance_line_ids:
            existen_emps.append(line.employee_id.id)
        if self.hr_employee_id.id:
            for line in self.balance_line_ids:
                if line.employee_id.hr_employee_id.id != self.hr_employee_id.id:
                    line.unlink()
        for employee in self.get_available_employees():
            if employee.id not in existen_emps:
                if employee.state_id.type in ('basis','trial','contract') and employee.contract_id and \
                        employee.contract_id.hour_balance_calculate_type:
                    values = self.get_balance_line_values(employee, employee.contract_id)
                    balance_line = hour_balance_line_obj.create(values)
                    balance_line.write({'origin': 'no_attendance'})
        for line in self.balance_line_ids:
            if line.origin == 'no_attendance':
                values = self.get_balance_line_values(line.employee_id, line.employee_id.contract_id)
                line.write(values)
                line.write({'origin': 'no_attendance'})
                line.compute_missed_hour()
            if line.employee_id.contract_id.ndsh_type.auto_call_on_hour_balance == True:
                line.write({'description': 'Жирэмсэн, хүүхдээ асарч буй эх'})
            if line.salary_type == 'advance_salary':
                line.sudo().calculate_advance_percent()

    # def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
    #     self.ensure_one()
    #
    #     date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
    #     period_date_from, period_date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date, is_balance=False)
    #     if not (date_from and date_to) or not (period_date_from and period_date_to):
    #         return {}
    #
    #     reg_hours = self.calc_reg_day(period_date_from, period_date_to, employee_id, contract_id)
    #     balance_reg_hours = self.calc_reg_day(date_from, date_to, employee_id, contract_id, is_balance=True)
    #
    #     if self.can_calc_reg_day(employee_id, contract_id):
    #         reg_day = reg_hours['working_day']
    #         reg_hour = reg_hours['working_hour']
    #         balance_reg_day = balance_reg_hours['working_day']
    #         balance_reg_hour = balance_reg_hours['working_hour']
    #     else:
    #         reg_day, reg_hour, balance_reg_day, balance_reg_hour = 0, 0, 0, 0
    #
    #     if date_from and date_to:
    #         attendance_hour, total_attendance_days = self.get_total_attendance_hours_days(employee_id, date_from,
    #                                                                                       date_to)  # Нийт ирцийн цаг/өдөр - Амралт нтр тооцохгүй. Ирсэн огноо болон гарсан огнооноос цайны цагийг хассан хугацаа байна
    #
    #         paid_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                        contract_id, date_from, date_to)
    #         unpaid_holiday = self.with_context({'balance_type': 'unpaid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                            contract_id, date_from, date_to)
    #         annual_leave = self.with_context({'balance_type': 'annual_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                                contract_id, date_from,
    #                                                                                                                                date_to)
    #         sick_leave_holiday = self.with_context({'balance_type': 'sick_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays',
    #                                                                                                                                    employee_id, contract_id,
    #                                                                                                                                    date_from, date_to)
    #         rehabilitated_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': True}).get_total_hours_by_object('hr.holidays', employee_id,
    #                                                                                                                                contract_id, date_from,
    #                                                                                                                                date_to)
    #     else:
    #         attendance_hour, total_attendance_days = 0, 0
    #         paid_holiday, unpaid_holiday, annual_leave, sick_leave_holiday, rehabilitated_holiday = 0, 0, 0, 0
    #
    #     values = {
    #         'origin': 'by_attendance',
    #         'employee_id': employee_id.id,
    #         'contract_id': contract_id.sudo().id,
    #         'employee_reg_number': employee_id.ssnid,
    #         'employee_last_name': employee_id.last_name,
    #         'department_id': self.department_id.id,
    #         'balance_id': self.id,
    #         'salary_type': self.salary_type,
    #         'reg_day': reg_day if reg_day > 0 else 0,
    #         'reg_hour': reg_hour if reg_hour > 0 else 0,
    #         'balance_reg_day': balance_reg_day if balance_reg_day > 0 else 0,
    #         'balance_reg_hour': balance_reg_hour if balance_reg_hour > 0 else 0,
    #         'attendance_hour': attendance_hour,
    #         'total_attendance_days': total_attendance_days,
    #         'paid_holiday': paid_holiday,
    #         'unpaid_holiday': unpaid_holiday,
    #         'annual_leave': annual_leave,
    #         'sick_leave': sick_leave_holiday,
    #         'rehabilitated_holiday': rehabilitated_holiday,
    #     }
    #     return values


class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"

    def _hr_holidays_money_nohon_count(self):
        for obj in self:
            start_date = obj.balance_id.balance_date_from + ' 00:00:00'
            stop_date = obj.balance_id.balance_date_to + ' 08:00:00'
            start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            hr_holidays_money_nohon_count_ids = self.env['moncon.hr.holidays'].search(
                [('employee_id', '=', obj.employee_id.id), ('date_from', '>=', start),
                 ('date_from', '<=', stop),('type', '=', 'money')])
            obj.hr_holidays_money_nohon_count = len(hr_holidays_money_nohon_count_ids)

    balance_id = fields.Many2one('hour.balance', string='Hour Balance', required=True)
    rehabilitated_money_holiday = fields.Float(string='Нөхөн амрах илүү цагаар тооцуулах цаг', digits=(4, 2))
    rehabilitated_add_holiday = fields.Float(string='Нөхөж амрах хуримтлагдсан цаг', digits=(4, 2))
    rehabilitated_holiday = fields.Float(string='Нөхөж амарсан цаг', digits=(4, 2))
    attendance_repair_count = fields.Integer(compute="_attendance_repair_count")
    hr_holidays_count = fields.Integer(compute='_hr_holidays_count')
    hr_holidays_nohon_count = fields.Integer(compute='_hr_holidays_nohon_count')
    hr_holidays_add_nohon_count = fields.Integer(compute='_hr_holidays_add_nohon_count')
    hr_holidays_money_nohon_count = fields.Integer(compute='_hr_holidays_money_nohon_count')

    balance_date_from = fields.Date(related='balance_id.balance_date_from')
    balance_date_to = fields.Date(related='balance_id.balance_date_to')
    total_worked_hour_balance = fields.Float(string='Хянах цаг', readonly=True, compute='compute_total_worked_hour_balance', digits=(4, 2))
    total_worked_hour = fields.Float(string='Total worked /hours/', readonly=True, compute='compute_total_worked_hour', digits=(4, 2))
    project_id = fields.Many2one(related='employee_id.project_id', store=True, readonly=False, string='Төсөл')

    check_attendance_month_field = fields.Float('Ирц', compute='compute_check_attendance_month_field')
    missed_hour_moncon = fields.Float(string='Таслалт/цаг/', compute='compute_missed_hour_moncon', store=True, readonly=False, digits=(4, 2))
    field_count_day = fields.Float(string='Талбайн хоног', digits=(4, 2))
    country_count_day = fields.Float(string='Хээрийн хоног', digits=(4, 2))
    advance_percent = fields.Float(string='Үндсэн батлагдсан цаг', digits=(4, 2), compute='calculate_advance_percent', store=True,  readonly=False)
    advance_day = fields.Float(string='Цагийн батлагдсан цаг', digits=(4, 2), compute='calculate_advance_percent', store=True,  readonly=False)
    advance_hour = fields.Float(string='Шувуу урьдчилгааны хувь', digits=(4, 2), compute='calculate_advance_percent', store=True,  readonly=False)
    advance_wage_hour = fields.Float(string='Батлагдсан өдөр', digits=(4, 2), compute='calculate_advance_percent', store=True,  readonly=False)
    is_close_salary = fields.Boolean(string='Цалин олголт хийхгүй эсэх')
    employee_name = fields.Char(related="employee_id.name")
    kpi_total_point = fields.Float(string='Нийт KPI')
    moncon_overtime = fields.Float(string='Илүү цаг (Шувуу төсөл)', readonly=True, compute='compute_moncon_overtime', store=True, digits=(4, 2))



    @api.multi
    def calculate_advance_percent(self):
        for obj in self:
            employee = self.env['hr.contract'].search([('employee_id', '=', obj.employee_id.id)], limit=1)
            if obj.salary_type == 'advance_salary':
                if obj.is_close_salary:
                    obj.advance_percent = 0
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 72 and employee.wage > 0:
                    obj.advance_percent = 40
                    obj.advance_hour = 0
                    obj.advance_day = 0
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 55 and (obj.worked_hour + obj.rehabilitated_holiday) < 72 and employee.wage > 0:
                    obj.advance_percent = 30
                    obj.advance_hour = 0
                    obj.advance_day = 0
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 72 and employee.time_salary > 0 and employee.wage == 0 and employee.kpi_time_salary == 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 40
                    obj.advance_day = 0
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 55 and (obj.worked_hour + obj.rehabilitated_holiday) < 72 and employee.time_salary > 0 and employee.wage == 0 and employee.kpi_time_salary == 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 30
                    obj.advance_day = 0
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 72 and employee.time_salary > 0 and employee.kpi_time_salary > 0 and employee.wage == 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
                    obj.advance_day = 45
                elif (obj.worked_hour + obj.rehabilitated_holiday) >= 55 and (obj.worked_hour + obj.rehabilitated_holiday) < 72 and employee.time_salary > 0 and employee.kpi_time_salary > 0 and employee.wage == 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
                    obj.advance_day = 35
                elif obj.worked_day > 2 and employee.hour_salary > 0:
                    obj.advance_wage_hour = obj.worked_day - 2
                    obj.advance_hour = 0
                    obj.advance_day = 0
                    obj.advance_percent = 0
                else:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
            elif obj.salary_type == 'last_salary':
                if obj.is_close_salary:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
                    obj.advance_day = 0
                    obj.advance_wage_hour = 0
                elif employee.wage > 0:
                    obj.advance_percent = obj.worked_hour
                    obj.advance_hour = 0
                    obj.advance_day = 0
                    obj.advance_wage_hour = 0
                elif employee.time_salary > 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
                    obj.advance_wage_hour = 0
                    obj.advance_day = obj.worked_hour
                elif employee.hour_salary > 0:
                    obj.advance_percent = 0
                    obj.advance_hour = 0
                    obj.advance_day = 0
                    obj.advance_wage_hour = obj.worked_day










    @api.depends('balance_reg_hour', 'total_worked_hour', 'paid_holiday', 'unpaid_holiday', 'annual_leave', 'sick_leave', 'out_working_hour',
                 'business_trip_hour', 'total_training_hours', 'delay_hour', 'unworked_leaved_hour')
    def compute_missed_hour_moncon(self):
        # ТАСЛАЛТ = БАЛАНСЫН АЖИЛЛАВАЛ ЗОХИХ /ЦАГ/ - ТОМИЛОЛТ – ГАДУУР АЖИЛ – СУРГАЛТ /ЦАГ/ - ЦАЛИНТАЙ ЧӨЛӨӨ /ЦАГ/ - ЭЭЛЖИЙН АМРАЛТ  /ЦАГ/ - ӨВЧНИЙ ЧӨЛӨӨ  /ЦАГ/ - ЦАЛИНГҮЙ ЧӨЛӨӨ  /ЦАГ/ - НИЙТ АЖИЛЛАСАН ЦАГ - Сул зогсолт - Ажиллаагүй болон ажлаас гарсан өдөр
        for obj in self:
            if obj.company_id.hr_contract_number == 1:
                holiday_hours = obj.paid_holiday + obj.unpaid_holiday + obj.annual_leave + obj.sick_leave + obj.rehabilitated_holiday
                missed_hour = obj.reg_hour - obj.worked_hour - obj.business_trip_hour - obj.delay_hour - holiday_hours - obj.lag_hour
                obj.missed_hour_moncon = max(missed_hour, 0)


    def compute_check_attendance_month_field(self):
        for obj in self:
            start_date = obj.balance_id.balance_date_from + ' 00:00:00'
            stop_date = obj.balance_id.balance_date_to + ' 08:00:00'
            start=get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop=get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            attendanc_repair_ids = self.env['hr.attendance'].search(
                [('employee_id', '=', obj.employee_id.id), ('check_in', '>=', start),
                 ('check_in', '<=', stop)])
            obj.check_attendance_month_field = len(attendanc_repair_ids)

    def check_attendance_month(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start=get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop=get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        attendanc_ids = self.env['hr.attendance'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('check_in', '>=', start),
             ('check_in', '<=', stop)])

        form_id = self.env.ref("l10n_mn_hr_attendance.hr_attendance_view_form")
        tree_id = self.env.ref("l10n_mn_hr_attendance.view_attendance_tree_inherit_attendance")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Ирц',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.attendance',
            'domain': [('id', 'in', attendanc_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def compute_total_worked_hour(self):
        for obj in self:
            employee = self.env['hr.contract'].search([('employee_id', '=', obj.employee_id.id)], limit=1)
            if employee.hour_salary > 0:
                obj.total_worked_hour = 0
            else:
                obj.total_worked_hour = obj.worked_hour + obj.rehabilitated_holiday

# Дахиад коммент хийх
    def compute_moncon_overtime(self):
        for obj in self:
            start_date = self.balance_id.balance_date_from + ' 00:00:00'
            stop_date = self.balance_id.balance_date_to + ' 08:00:00'
            start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            attendanc_ids = self.env['hr.attendance'].search(
                [('employee_id', '=', obj.employee_id.id), ('check_in', '>=', start),
                 ('check_in', '<=', stop),('over_time_hours', '<=', 1)])
            count_more1_attendanc_ids = self.env['hr.attendance'].search(
                [('employee_id', '=', obj.employee_id.id), ('check_in', '>=', start),
                 ('check_in', '<=', stop),('over_time_hours', '>', 1)])
            country_employee_name = []
            count_moncon_overtime = 0
            count_moncon_overtime_more_one = 0
            employee_country_ids = self.env['hr.contract'].sudo().search(
                [('employee_id', '=', obj.employee_id.id), ('is_moncon_overtime_limit', '=', 'True')])
            for country_contract_employee_id in employee_country_ids:
                country_employee_name.append(country_contract_employee_id.employee_id.id)
            for att in attendanc_ids:
                if att.employee_id.id in country_employee_name:
                    count_moncon_overtime += att.over_time_hours
            for att in count_more1_attendanc_ids:
                if att.employee_id.id in country_employee_name:
                    count_moncon_overtime_more_one += 1
            obj.moncon_overtime = count_moncon_overtime + count_moncon_overtime_more_one
            if obj.employee_id.contract_id.total_moncon_holiday_limit < (count_moncon_overtime + count_moncon_overtime_more_one):
                obj.moncon_overtime = obj.employee_id.contract_id.total_moncon_holiday_limit
            else:
                obj.moncon_overtime = (count_moncon_overtime + count_moncon_overtime_more_one)



    def compute_field_count_day(self):
        for obj in self:
            start_date = self.balance_id.balance_date_from + ' 08:00:00'
            stop_date = self.balance_id.balance_date_to + ' 15:59:59'
            start = get_display_day_to_user_day(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            attendanc_ids = self.env['hr.attendance'].sudo().search(
                [('employee_id', '=', obj.employee_id.id), ('check_in', '>=', start),
                ('check_in', '<=', stop)])
            country_employee_name = []
            field_employee_name = []
            field_count_day = 0
            country_count_day = 0
            employee_country_ids = self.env['hr.contract'].sudo().search([('employee_id', '=', obj.employee_id.id), ('country_addition', '>', 0)])
            for country_contract_employee_id in employee_country_ids:
                country_employee_name.append(country_contract_employee_id.employee_id.id)
            employee_field_ids = self.env['hr.contract'].sudo().search([('employee_id', '=', obj.employee_id.id), ('field_addition', '>', 0)])
            for field_contract_employee_id in employee_field_ids:
                field_employee_name.append(field_contract_employee_id.employee_id.id)
            for att in attendanc_ids:
                if att.employee_id.id in country_employee_name and att.total_worked_hours_of_date >= 6:
                    country_count_day += 1
                if att.employee_id.id in field_employee_name and att.total_worked_hours_of_date >= 6:
                    field_count_day +=1
            obj.field_count_day = field_count_day
            obj.country_count_day = country_count_day


    def compute_total_worked_hour_balance(self):
        for obj in self:
            obj.total_worked_hour_balance = obj.worked_hour + obj.paid_holiday + obj.lag_hour + obj.business_trip_hour + obj.delay_hour


    def _attendance_repair_count(self):
        for obj in self:
            attendanc_repair_ids = self.env['hr.attendance.repair'].search([('employee_id', '=', obj.employee_id.id), ('datetime', '>=', obj.balance_id.balance_date_from), ('datetime', '<=', obj.balance_id.balance_date_to)])
            obj.attendance_repair_count = len(attendanc_repair_ids)


    def _hr_holidays_count(self):
        for obj in self:
            start_date = obj.balance_id.balance_date_from + ' 00:00:00'
            stop_date = obj.balance_id.balance_date_to + ' 08:00:00'
            start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            hr_holidays_ids = self.env['hr.holidays'].search([('employee_id', '=', obj.employee_id.id), ('date_from', '>=', start),
                                                                            ('date_from', '<=', stop)])
            obj.hr_holidays_count = len(hr_holidays_ids)


    def _hr_holidays_nohon_count(self):
        for obj in self:
            start_date = obj.balance_id.balance_date_from + ' 00:00:00'
            stop_date = obj.balance_id.balance_date_to + ' 08:00:00'
            start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            hr_holidays_nohon_count_ids = self.env['moncon.hr.holidays'].search(
                [('employee_id', '=', obj.employee_id.id), ('date_from', '>=', start),
                 ('date_from', '<=', stop), ('type', '=', 'remove')])
            obj.hr_holidays_nohon_count = len(hr_holidays_nohon_count_ids)


    def _hr_holidays_add_nohon_count(self):
        for obj in self:
            start_date = obj.balance_id.balance_date_from + ' 00:00:00'
            stop_date = obj.balance_id.balance_date_to + ' 08:00:00'
            start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
            stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
            hr_holidays_add_nohon_count_ids = self.env['moncon.hr.holidays'].search(
                [('employee_id', '=', obj.employee_id.id), ('date_from', '>=', start),
                 ('date_from', '<=', stop),('type', '=', 'add')])
            obj.hr_holidays_add_nohon_count = len(hr_holidays_add_nohon_count_ids)


    def check_hr_holidays(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        hr_holidays_ids = self.env['hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_from', '>=', start),
             ('date_from', '<=', stop),  ('holiday_status_id.is_nohon_amrakh', '=', False)])

        tree_id = self.env.ref("l10n_mn_hr_hour_balance.view_holiday_employee_inherit")
        form_id = self.env.ref("l10n_mn_hr_hour_balance.view_holiday_form_inherit")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Амралт чөлөө',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def check_hr_holidays_nohon_amrakh(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        hr_holidays_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),
             ('date_from', '>=', start),
             ('date_from', '<=', stop),
             ('type', '=', 'remove')])


        tree_id = self.env.ref("l10n_mn_moncon_hr_holidays.view_moncon_hr_holidays_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Нөхөн амрах хоног биеэр эдлэх',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'moncon.hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def check_hr_holidays_add_nohon_amrakh(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        hr_holidays_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),
             ('date_from', '>=', start),
             ('date_from', '<=', stop),
             ('type', '=', 'add')])


        tree_id = self.env.ref("l10n_mn_moncon_hr_holidays.view_moncon_hr_holidays_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view")


        return {
            'type': 'ir.actions.act_window',
            'name': u'Нөхөн амрах хоног хуримтлуулах',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'moncon.hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def check_hr_holidays_money_nohon_amrakh(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        hr_holidays_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),
             ('date_from', '>=', start),
             ('date_from', '<=', stop),
             ('type', '=', 'money')])
        tree_id = self.env.ref("l10n_mn_moncon_hr_holidays.view_moncon_hr_holidays_tree")
        form_id = self.env.ref("l10n_mn_moncon_hr_holidays.moncon_hr_holidays_view")
        return {
            'type': 'ir.actions.act_window',
            'name': u'Нөхөн амрах хоног илүү цагаар',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'moncon.hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }

    def confirm(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        attendanc_repair_ids = self.env['hr.attendance.repair'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('datetime', '>=', start),
             ('datetime', '<=', stop)])
        hr_holidays_ids = self.env['hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_from', '>=', start),
             ('date_from', '<=', stop), ('holiday_status_id.is_nohon_amrakh', '=', False)])
        hr_holidays_nuhun_amarsan_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),('date_from', '>=', start),('date_from', '<=', stop),('type', '=', 'remove')])
        hr_holidays_nuhun_add_amarsan_ids = self.env['moncon.hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id),('date_from', '>=', start),('date_from', '<=', stop),('type', '=', 'add')])
        for obj in attendanc_repair_ids:
            if obj.state == 'send':
                raise ValidationError(u'Танд бүргэгдээгүй ирц нөхөн засах хүсэлт байна, бүртгүүлнэ үү!')
        for obj in hr_holidays_ids:
            if obj.state == 'confirm':
                raise ValidationError(u'Танд бүргэгдээгүй амралт чөлөөний хүсэлт байна, бүртгүүлнэ үү!')
        for obj in hr_holidays_nuhun_amarsan_ids:
            if obj.state == 'send':
                raise ValidationError(u'Танд бүргэгдээгүй нөхөн амарсан хүсэлт байна, бүртгүүлнэ үү!')
        for obj in hr_holidays_nuhun_add_amarsan_ids:
            if obj.state == 'send':
                raise ValidationError(u'Танд бүргэгдээгүй нөхөн амрах хуримтлуулах хүсэлт байна, бүртгүүлнэ үү!')

        attendanc_ids = self.env['hr.attendance'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('check_in', '>=', start),
             ('check_in', '<=', stop)])
        for attendanc_id in attendanc_ids:
            if not attendanc_id.total_worked_hours_of_date and attendanc_id.work_start_time and attendanc_id.total_worked_hours_of_date == 0:
                raise ValidationError(u'Танд ажиллсан цаг байхгүй ирц байна, Ирцээ шалгана үү!')


        self.sudo().state = 'sent'


    def draft(self):
        self.sudo().state = 'draft'

    def check_attendance(self):
        start_date = self.balance_id.balance_date_from + ' 00:00:00'
        stop_date = self.balance_id.balance_date_to + ' 08:00:00'
        start = get_day_to_display_timezone(start_date, self.env.user).strftime('%Y%m%d_%H%M')
        stop = get_day_like_display(stop_date, self.env.user).strftime('%Y%m%d_%H%M')
        attendanc_repair_ids = self.env['hr.attendance.repair'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('datetime', '>=', start),
             ('datetime', '<=', stop)])

        tree_id = self.env.ref("l10n_mn_hr_attendance_repair.view_attendance_repair_tree")
        form_id = self.env.ref("l10n_mn_hr_attendance_repair.hr_attendance_repair_form")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Ирц нөхөн засвар',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.attendance.repair',
            'domain': [('id', 'in', attendanc_repair_ids.ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current'
        }


    def update_line(self):
        if self.company_id.hr_contract_number == 1:
            contract = self.employee_id.contract_id
            if contract and contract.active:
                values = self.balance_id.get_balance_line_values(self.employee_id, contract)
                # if self.balance_id.can_insert_balance_line(self.employee_id, contract, self.balance_id.get_active_hours(values)):
                    # self.sudo().write(values)
                self.sudo().action_import_kpi_line()
                self.sudo().calculate_advance_percent()
                self.sudo().write(values)
                self.sudo().compute_total_worked_hour()
                self.sudo().compute_moncon_overtime()



    @api.multi
    def action_import_kpi_line(self):
        for obj in self:
            kpi_employee_id = self.env['moncon.kpi.line'].search([('period_id', '=', obj.period_id.id), ('employee_id', '=', obj.employee_id.id),
                     ('kpi_line_state1', '=', 'done'), ('kpi_line_state2', '=', 'done')])
            if kpi_employee_id:
                obj.kpi_total_point = kpi_employee_id.total_kpi
            else:
                return False

