# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Recruitment",
    'description': """
       Human Resource Additional Features of Recruitment
    """,
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_recruitment',
    ],
    'data': [
    ],
    'installable': True,
}
