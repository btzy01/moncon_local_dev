# -*- coding: utf-8 -*-
import time
import datetime

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class PaymentDate(models.TransientModel):
    _name = "payment.date"
    _description = "Payment Date"

    @api.model
    def default_get(self, default_fields):
        context = dict(self._context or {})
        res = super(PaymentDate, self).default_get(default_fields)
        if context['active_id']:
            payment = self.env['payment.certificate'].browse(context['active_id'])
            if 'date' in default_fields:
                res['date'] = payment.pay_date
        return res

    date = fields.Date(string='Төлбөрийн огноо', required=1)

    def add(self):
        context = dict(self._context or {})
        if context['active_id']:
            payment = self.env['payment.certificate'].browse(context['active_id'])
            if payment:
                payment.state = 'fee'
                transaction_obj = self.env['payment.permit']
                today = self.date
                permissions = self.env['transaction.payment.permission'].search([('date', '=', today), ('company_id', '=', self.env.user.company_id.id)], limit=1)
                if permissions:
                    permission = permissions[0]
                else:
                    permission = self.env['transaction.payment.permission'].create({'date': today, 'company_id': self.env.user.company_id.id})
                existings = transaction_obj.search([('cert_id', '=', payment.id), ('state', '=', 'draft'), ('transaction_id.date', '=', today)])
                if existings:
                    # Хэрэв payment permit дээр бичилт үүссэн бол
                    existings.write({'cert_id': payment.id,
                                     'company_id': payment.company_id.id,
                                     'descrip': str(payment.name) or '',
                                     'date': today,
                                     'res_partner': payment.partner_id.id,
                                     'total_amount': payment.other_amount_without_tax,
                                     'currency': payment.currency_id.id,
                                     'date_end': payment.pay_date,
                                     'remaining_value': payment.residual,
                                     'paid_value': 0,
                                     'pay_value': payment.currency_amount,
                                     'contract_id': payment.contract_id.id or False,
                                     'analytic_account_id': payment.expense_category.id or False,
                                     'analytic_account_project_id': payment.expense_category.id or False,
                                     'invoice_date': payment.pay_date,
                                     'comment': payment.note,
                                     })
                else:
                    transaction = self.env['payment.permit'].create({'name': payment.name,
                                                                     'cert_id': payment.id,
                                                                     'company_id': payment.company_id.id,
                                                                     'descrip': str(payment.name) or '',
                                                                     'date': today,
                                                                     'res_partner': payment.partner_id.id,
                                                                     'state': 'draft',
                                                                     'currency': payment.currency_id.id,
                                                                     'date_end': payment.pay_date,
                                                                     'remaining_value': payment.residual,
                                                                     'paid_value': 0,
                                                                     'pay_value': payment.currency_amount,
                                                                     'total_amount': payment.other_amount_without_tax,
                                                                     'transaction_id': permission.id,
                                                                     'contract_id': payment.contract_id.id or False,
                                                                     'analytic_account_id': payment.expense_category.id or False,
                                                                     'analytic_account_project_id': payment.expense_category.id or False,
                                                                     'invoice_date': payment.pay_date,
                                                                     'comment': payment.note,
                                                                     'allow_payment_types': payment.allow_payment_types,
                                                                     })