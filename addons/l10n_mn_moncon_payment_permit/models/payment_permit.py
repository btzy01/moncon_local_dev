# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class PaymentPermit(models.Model):
    _inherit = 'payment.permit'

    cert_id = fields.Many2one('payment.certificate', string='Төлбөрийн хуудас')
    allow_payment_types = fields.Selection([('1', 'Бараа материал'),
                                            ('2', 'Гэрээт ажлын гүйцэтгэл'),
                                            ('3', 'Төслийн жижиг зардал'),
                                            ('4', 'ҮА удирдлагын зардал')], string='ТЗХ төрөл', required=True, default='1')

    @api.multi
    def pay(self):
        res = super(PaymentPermit, self).pay()
        if self.cert_id:
            self.cert_id.amount_pay = self.cert_id.amount_pay + self.pay_value
            self.cert_id.onchange_other_amount_without_tax()
            if self.cert_id.residual == 0:
                self.cert_id.state = 'complete'
        return res

    @api.multi
    def cancel(self):
        res = super(PaymentPermit, self).cancel()
        if self.cert_id:
            self.cert_id.write({'state': 'finished'})
        return res

    @api.multi
    def button_show_certification(self):
        for obj in self:
            if obj.cert_id:
                return {
                    'name': _('Төлбөрийн хүсэлт'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'payment.certificate',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('id', '=', obj.cert_id.id)],
                }
