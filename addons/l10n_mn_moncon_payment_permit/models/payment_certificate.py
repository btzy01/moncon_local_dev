# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class PaymentCertificate(models.Model):
    _inherit = 'payment.certificate'

    @api.multi
    def payment_count(self):
        for obj in self:
            p_ids = self.env['payment.permit'].search([('cert_id', '=', obj.id)])
            obj.pay_count = len(p_ids)

    pay_count = fields.Integer(string='Төлбөр', compute='payment_count')

    @api.multi
    def stat_button_transaction_payment(self):
        for obj in self:
            t_ids = []
            pay = self.env['payment.permit'].search([('cert_id', '=', obj.id)])
            for p in pay:
                tran = self.env['transaction.payment.permission'].search([('id', '=', p.transaction_id.id)])
                for t in tran:
                    t_ids.append(t.id)
            return {
                'type': 'ir.actions.act_window',
                'name': u'Гарах төлбөрийн зөвшөөрөл',
                'res_model': 'transaction.payment.permission',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', t_ids)],
            }