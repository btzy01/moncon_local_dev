# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    "name" : "Moncon Payment Permission",
    "version" : "1.0",
    "author" : "Moncon LLC",
    "description": """Payment Permission""",
    "website" : "https://moncon.mn",
    "category" : "Payment",
    "depends" : ['l10n_mn_payment_permit', 'l10n_mn_moncon_tzh'],
    "init": [],
    "data" : [
        'wizard/transaction_view.xml',
        'wizard/payment_date_view.xml',
        'views/menu_view.xml',
        'views/payment_permit_view.xml',
    ],
    'license': 'GPL-3',
    "active": False,
    "installable": True,
}
