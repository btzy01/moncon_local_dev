# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - KPI',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    "description": """
     Уг модуль нь Монконы хүний нөөцийн KPI үнэлгээг тооцоолно.
 """,
    'website': 'http://www.asterisk-tech.mn',
    'images': [''],
    'depends': ['hr','l10n_mn_account_period', 'l10n_mn_hr'],
    'data': [
        'data/sequence.xml',
        'data/mail_template.xml',
        'data/cron.xml',
        'security/data.xml',
        'security/ir.model.access.csv',
        'report/print_kpi_report.xml',
        'views/moncon_kpi_view.xml',
        'email_templates/kpi_send_email_template.xml'
    ],
    'installable': True,
    'auto_install': False
}


