# -*- coding: utf-8 -*-

from odoo import api, models

class PrintKpiReport(models.Model):
    _name = "report.l10n_mn_moncon_kpi.print_kpi_report"

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        moncon_kpi_obj = self.env['moncon.kpi']
        report = report_obj._get_report_from_name('l10n_mn_moncon_kpi.print_kpi_report')
        kpi_employeee_ids = moncon_kpi_obj.browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': kpi_employeee_ids,
        }
        return report_obj.with_context(self._context).render('l10n_mn_moncon_kpi.print_kpi_report', docargs)

