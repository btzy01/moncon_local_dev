# -*- coding: utf-8 -*-
{
    'name': "Moncon - Purchase Move",
    'depends': [
        'l10n_mn_purchase_extra_cost',
        'l10n_mn_stock_account_create_move',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """Журналын бичилтийг худалдан авалттай холбох.""",
    'data': [
        'views/account_move_line_views.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False
}
