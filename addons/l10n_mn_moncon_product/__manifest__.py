# -*- coding: utf-8 -*-
{
    'name': "Moncon Product",
    'version': '1.0',
    'depends': [
        'purchase','l10n_mn_product_expense'
    ],
    'author': "",
    'category': 'Moncon Product',
    'description': """
         Moncon-Бараа материалын бүртгэл.
    """,
    'website': '',
    'data': [
        'views/product_template_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}