# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from os import path
import time
from datetime import timedelta
from datetime import datetime
from dateutil import parser
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _  # @UnresolvedImport.
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from odoo.addons.l10n_mn_web.models.time_helper import *

_logger = logging.getLogger('odoo')
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"

_logger = logging.getLogger(__name__)


class MonconHrProject(models.Model):
    _inherit = 'worked.project'

    employee_id = fields.Many2one("hr.employee", required=1)
    project_id = fields.Many2one("project.project", required=1)


class MonconHrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.one
    def _hide_personal_information(self):
        if self.env.user.has_group('hr.group_hr_manager') or (self.user_id and self.env.uid == self.user_id.id):
            self.hide_personal_information = False
        else:
            self.hide_personal_information = True

    rel_person_name2 = fields.Char(String='Rel person 2', digits=64)
    rel_person_phone2 = fields.Char(String='Rel phone 2', digits=64)
    employee_grade = fields.Many2one('employee.grade', String='Employee grade')
    fired_date_moncon = fields.Date(String='Fired date')
    fired_order_num_moncon = fields.Char(String='Fired order number')
    fired_type_moncon = fields.Selection([
        ('self_decision', 'Self decision'),
        ('date_expired', 'Date expired'),
        ('company_decision', 'Company decision')], string='Fired type')
    fired_cause_moncon = fields.Many2one('employee.cause', String='Fired cause')
    fired_is = fields.Boolean(String='Is fired', compute='_fired_is')
    kpi_check =  fields.Boolean('Kpi ашиглах', default=False)
    kpi_percent_weight1 = fields.Float(string='KPI Weight 1', digits=(3, 2))
    approve_user_id1 = fields.Many2one('res.users', string='User1 to approve')
    kpi_percent_weight2 = fields.Float(string='KPI Weight 2', digits=(3, 2))
    approve_user_id2 = fields.Many2one('res.users', string='User2 to approve')
    project_id = fields.Many2one("project.project", string='Project in')
    shirt_size = fields.Selection(selection_add=[('38', '38'),
                                                 ('40', '40'),
                                                 ('42', '42'),
                                                 ('44', '44'),
                                                 ('46', '46'),
                                                 ('48', '48')
                                                 ])
    sitting_drums_size = fields.Selection(selection_add=[('38', '38'),
                                                         ('40', '40'),
                                                         ('42', '42'),
                                                         ('44', '44'),
                                                         ('46', '46'),
                                                         ('48', '48'),
                                                         ('50', '50'),
                                                         ('52', '52')
                                                         ])

    engagement_in_company = fields.Date(string='Date of Engagement in company', store=True, required=True)
    department_hr_tag_ids = fields.Many2one('department.hr.tag',  string="Tags")
    working_hours = fields.Many2one(related='contract_id.working_hours', string='Working Schedule')
    hr_employee_id = fields.Many2one('hr.employee', u'ХН ажилтан')

    @api.multi
    def _inverse_remaining_leaves(self):
        status_list = self.env['hr.holidays.status'].search([('limit', '=', False)])

    @api.depends('department_id')
    @api.onchange('department_id')
    def onchange_department(self):
        if self.department_id:
            return {'domain': {'job_id': []}}

    # KPI Үнэлгээ 100- аас их, 0-ээс бага байвал хадгалах үед анхааруулга өгөх.
    @api.constrains('kpi_percent_weight1', 'kpi_percent_weight2')
    def _check_kpi_value_percent(self):
        for line in self:
            if line.kpi_percent_weight1 > 100 or line.kpi_percent_weight1 < 0:
                raise ValidationError(_('Error ! KPI Percent must be between 0 to 100!!!.'))
            elif line.kpi_percent_weight2 > 100 or line.kpi_percent_weight2 < 0:
                raise ValidationError(_('Error ! KPI Percent must be between 0 to 100!!!.'))
            else:
                return False

    @api.depends('state_id.type')
    def _fired_is(self):
        for emp in self:
            if emp.state_id.type == 'resigned':
                emp.fired_is = True
            else:
                emp.fired_is = False

    def get_birthday_guys(self, day):
        current_date = datetime.strptime(str(day)[:10], '%Y-%m-%d')
        self._cr.execute('SELECT id FROM hr_employee WHERE EXTRACT(month from birthday) = %s AND EXTRACT(day from birthday) = %s' % (current_date.month, current_date.day))
        emloyee_ids = map(lambda x: x[0], self._cr.fetchall())
        return self.env['hr.employee'].browse(emloyee_ids) if emloyee_ids else False

    @api.multi
    def _cron_emp_birthday_reminder(self):
        # Өнөөдөр, маргааш төрсөн өдөр нь тохиож буй хүмүүсийг олох
        todays_bday_guys = self.get_birthday_guys(get_day_like_display(datetime.now(), self.env.user))
        tomorrows_bday_guys = self.get_birthday_guys(get_day_like_display(datetime.now() + relativedelta(days=1), self.env.user))

        def get_belown_guys(user_id):
            emp = self.env['hr.employee'].sudo().search([('user_id', '=', user_id.sudo().id)])
            todays_belown_guys, tomorrows_belown_guys = self.env['hr.employee'], self.env['hr.employee']
            if user_id.has_group('hr_recruitment.group_hr_recruitment_manager'):
                todays_belown_guys = todays_bday_guys.filtered(
                    lambda x: x.department_id in user_id.sudo().allowed_department_ids)
                tomorrows_belown_guys = tomorrows_bday_guys.filtered(
                    lambda x: x.department_id in user_id.sudo().allowed_department_ids)
            todays_belown_guys |= todays_bday_guys.filtered(
                lambda x: x.parent_id == emp and x not in todays_belown_guys)
            tomorrows_belown_guys |= tomorrows_bday_guys.filtered(
                lambda x: x.parent_id == emp and x not in tomorrows_belown_guys)
            return todays_belown_guys, tomorrows_belown_guys

        def get_detailed_info(employee_ids):
            detailed_info = []
            if employee_ids:
                index = 0
                for employee_id in employee_ids:
                    index += 1
                    detailed_info.append({
                        'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                        'db_name': self.env.cr.dbname,
                        'id': employee_id.id,
                        'action_id': self.env['ir.model.data'].get_object_reference('hr', 'open_view_employee_list')[1],
                        'model': 'hr.employee',
                        'index': index,
                        'name': employee_id.name,
                        'last_name': employee_id.last_name,
                        'birthday': employee_id.birthday,
                        'department': employee_id.department_id.name,
                        'job': employee_id.job_id.name
                    })
            return detailed_info

        if todays_bday_guys or tomorrows_bday_guys:
            # Мэйл илгээх эрх бүхий хүмүүсийг олох
            hr_manager_group = self.env['ir.model.data'].sudo().get_object('hr_recruitment',
                                                                           'group_hr_recruitment_manager')
            user_ids = self.env['res.users'].sudo().search([('groups_id', 'in', hr_manager_group.id)])
            for user in todays_bday_guys.mapped('parent_id').mapped('resource_id').mapped('user_id') | tomorrows_bday_guys.mapped('parent_id').mapped('resource_id').mapped(
                    'user_id'):
                if user not in user_ids:
                    user_ids |= user

            if user_ids:
                for user_id in user_ids:
                    todays_belown_guys, tomorrows_belown_guys = get_belown_guys(user_id.sudo())
                    todays_detailed_info = get_detailed_info(todays_belown_guys)
                    tomorrows_detailed_info = get_detailed_info(tomorrows_belown_guys)

                    if todays_detailed_info or tomorrows_detailed_info:
                        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_hr', 'contract_emp_birthday_reminder_template')[1]
                        template = self.env['mail.template'].sudo().browse(template_id)
                        template.with_context({'todays_detailed_info': todays_detailed_info, 'tomorrows_detailed_info': tomorrows_detailed_info}).send_mail(user_id.id,
                                                                                                                                                            force_send=True)

    @api.model
    def _cron_emp_birthday_greeting_sender(self):
        # Төрсөн өдөр нь тохиож буй ажилтанд мэндчилгээ илгээнэ
        folder = path.dirname(path.realpath(__file__))
        image_path = folder + "/../static/src/img/HBD.png"
        _logger.info("\n\n\n\n\nimage_path:\t%s###########" % str(image_path))
        todays_bday_guys = self.get_birthday_guys(get_day_like_display(datetime.now(), self.env.user))
        _logger.info("%s#############" % str(todays_bday_guys.mapped('address_home_id').mapped('id')))
        _logger.info("%s#############\n\n\n\n" % path.isfile(image_path))
        if path.isfile(image_path):
            todays_bday_guys = self.get_birthday_guys(get_day_like_display(datetime.now(), self.env.user))
            if todays_bday_guys:
                partner_ids = todays_bday_guys.mapped('address_home_id').mapped('id')
                if partner_ids:
                    for partner_id in partner_ids:
                        template = self.env.ref('l10n_mn_moncon_hr.contract_emp_birthday_greeting_sender_template')
                        template.with_context({'image_path': image_path}).send_mail(partner_id, force_send=True)


class hr_employee_grade(models.Model):
    _name = 'employee.grade'
    _rec_name = 'name'

    code = fields.Char('Code', size=128, required=True)
    name = fields.Char('Name', size=128, required=True)


class hr_employee_grade(models.Model):
    _name = 'employee.cause'
    _rec_name = 'name'

    code = fields.Char('Code', size=128, required=True)
    name = fields.Char('Name', size=128, required=True)


class hr_language(models.Model):
    _inherit = 'hr.language'

    listening_skill = fields.Selection(selection_add=[('beginning', u'Анхан'),
                                                      ('intermediate_low', u'Дундаас доогуур'),
                                                      ('intermediate', u'Дунд'),
                                                      ('intermediate_above', u'Дундаас дээгүүр'),
                                                      ('advanced', u'Гүнзгий')],
                                       string='Listening', required=True)


class DepartmentHrTag(models.Model):
    _name = 'department.hr.tag'

    name = fields.Char(string='Нэр', required=True, translate=True)
    company_id = fields.Many2one('res.company', string='Компани', required=True, default=lambda self: self.env.user.company_id, readonly=True)
    manager_id = fields.Many2one('res.users', 'Manager')#Гүйцэтгэх удирдлага

    workflow_count = fields.Integer(compute='_compute_workflow_count', string='Ажлын урсгал')

    @api.multi
    def button_workflow(self):
        return {
            'name': _('Workflow'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'workflow.config',
            'type': 'ir.actions.act_window',
            'domain': [('workflow_tag_id', 'in', self.ids)],
        }

    @api.multi
    def _compute_workflow_count(self):
        workflow_obj = self.env['workflow.config']
        for tag in self:
            workflow_ids = workflow_obj.search([('workflow_tag_id', 'in', tag.ids), ('active', '=', True)])
            tag.workflow_count = len(workflow_ids)

    @api.multi
    def write(self, vals):
        if 'manager_id' in vals:
            workflow_ids = self.env['workflow.config'].search([('workflow_tag_id', '=', self.id)])
            for workflow in workflow_ids:
                for line in workflow.line_ids:
                    line.write({'user_id': vals['manager_id']})

                attendance_repair_ids = self.env['hr.attendance.repair'].search([('workflow_id', '=', workflow.id),('state', '=', 'send')])
                for attendance_repair_id in attendance_repair_ids:
                    history = self.env['workflow.history'].search([('attendance_repair_id', '=', attendance_repair_id.id)], order='sent_date desc', limit=1)
                    history.write({'user_ids': [(4, vals['manager_id'])]})

                moncon_holidays_ids = self.env['moncon.hr.holidays'].search([('workflow_id', '=', workflow.id),('state', '=', 'send')])
                for moncon_holidays_id in moncon_holidays_ids:
                    moncon_history = self.env['moncon.hr.holidays.workflow.history'].search([('history', '=', moncon_holidays_id.id)], order='sent_date desc', limit=1)
                    moncon_history.write({'user_ids': [(4, vals['manager_id'])]})

                holidays_ids = self.env['hr.holidays'].search([('workflow_id', '=', workflow.id),('state', '=', 'confirm')])
                for holidays_id in holidays_ids:
                    holiday_history = self.env['hr.holidays.workflow.history'].search([('history', '=', holidays_id.id)], order='sent_date desc', limit=1)
                    holiday_history.write({'user_ids': [(4, vals['manager_id'])]})


        return super(DepartmentHrTag, self).write(vals)

class hr_software_skill(models.Model):
    _inherit = 'hr.software.skill'

    software_skill = fields.Selection([('low', u'Анхан'),
                                       ('intermediate_low', u'Дундаас доогуур'),
                                       ('intermediate', 'Дунд'),
                                       ('intermediate_above', 'Дундаас дээгүүр'),
                                       ('good', u'Сайн'),
                                       ('excellent', u'Маш сайн')],
                                      string='Software skill', required=True)


class HrDepartment(models.Model):
    _inherit = "hr.department"

    workflow_count = fields.Integer(compute='_compute_workflow_count', string='Ажлын урсгал')

    @api.multi
    def button_workflow(self):
        return {
            'name': _('Workflow'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'workflow.config',
            'type': 'ir.actions.act_window',
            'domain': [('department_id', 'in', self.ids)],
        }

    @api.multi
    def _compute_workflow_count(self):
        workflow_obj = self.env['workflow.config']
        dep_obj = self.env['hr.department']
        for department in self:
            dep_objs = dep_obj.search([('id', '=', department.id)])
            dep_ids = dep_objs.ids
            workflow_ids = workflow_obj.search([('department_id', 'in', dep_ids), ('active', '=', True)])
            department.workflow_count = len(workflow_ids)
