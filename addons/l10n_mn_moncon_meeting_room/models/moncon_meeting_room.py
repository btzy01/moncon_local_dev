# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import date, datetime, timedelta
from odoo.http import request


class MonconMeetingRoom(models.Model):

    _name = 'moncon.meeting.room'
    _description = u'Хурлын өрөө'
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    note = fields.Char(string=u'Тайлбар', size=256, track_visibility='onchange', required=True )
    date_start = fields.Datetime('Эхлэх огноо')
    date_stop = fields.Datetime('Дуусах огноо')
    meeting_room_type = fields.Selection([('meeting_room', u'Хурлын өрөө'),
                                          ('yellow_room_1', u'yellow room 1'),
                                          ('yellow_room_2', u'yellow room 2'),
                                          ('yellow_room_3', u'yellow room 3'),
                                          ('floor_13', u'13 Давхар')], u'Өрөөний төрөл', default='meeting_room', required=True )
    state = fields.Selection([('draft', 'Ноорог'),
                              ('confirmed', 'Батласан')], 'Төлөв', default='draft', required=True, track_visibility='onchange')


    employee_ids = fields.One2many('hr.employee.meeting', 'room_id', string='Ажилтан', required=True)

    @api.multi
    def action_send(self):
        for obj in self:
            # Хуралд оролцох ажилтанд майл илгээх
            subject = u"Хурал товлогдсон байна."
            if self.employee_ids:
                emails = []
                mail_send = []
                email_receivers = []
                object_name = unicode('moncon.meeting.room')
                db_name = request.session['db']
                base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                outgoing_email = self.env['ir.mail_server'].sudo().search([])
                current_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + unicode(
                    self.id) + '&view_type=form&model=' + object_name
                if not self.env.user.sudo().partner_id.sudo().email:
                    raise UserError(_(u"Танд мэйл бүртгэгдээгүй байна!!!"))
                if not outgoing_email:
                    raise UserError(_(
                        'There is no configuration for outgoing mail server. Please contact system administrator.'))
                else:
                    for receiver in self.employee_ids:
                        if receiver.employee_id not in mail_send:
                            if not receiver.employee_id.work_email:
                                raise UserError(_(u"Ажилтаны майл хаяг тохируулаагүй байна."))
                            outgoing_email = self.env['ir.mail_server'].sudo().search(
                                [('id', '=', outgoing_email[0].id)])

                            body_html = u'''<p>Сайн байна уу, ''' + unicode(receiver.employee_id.sudo().name) + u'''<br/></p>
                                             <p> Танд <b>''' + u'</b> хэрэглэгчийн <b>''' + self.note + u'''</b> тайлбартай хурал товлогдсон тухай мэдэгдэл ирлээ.<br/></p>
                                             <b><a href="''' + unicode(current_url) + u'''" target="_blank" class="mybtn">Дэлгэрэнгүй холбоос</a></b>
                                             <br/> -- <br/>
                                             <div><p>Баярлалаа,<br/><br/> -- <br/>ERP Автомат Имэйл </p></div>'''
                            vals = {
                                'state': 'outgoing',
                                'subject': subject,
                                'body_html': body_html,
                                'email_to': receiver.employee_id.work_email,
                                'email_from': self.env.user.sudo().partner_id.sudo().email,
                            }
                            emails.append(self.env['mail.mail'].create(vals))
                if emails:
                    for email in emails:
                        email.send()
                        email_receivers.append(email.sudo().email_to)

                    if email_receivers and len(email_receivers) > 0:
                        self.message_post(body=u'Дараах хэрэглэгч рүү захиалгыг цуцалсан тухай имэйл илгээгдэв: ' + ', '.join(email_receivers))
                else:
                    raise UserError(_('There is problem with emails'))

            return self.write({'state': 'confirmed'})

    def action_draft(self):
        self.state = 'draft'


    @api.constrains('date_start', 'date_stop')
    def _check_date_meeting(self):
        for meeting in self:
            domain = [
                ('date_start', '<=', meeting.date_start),
                ('date_stop', '>=', meeting.date_start),
                ('date_stop', '>=', meeting.date_stop),
                ('date_start', '<=', meeting.date_stop),
                ('meeting_room_type', '=', meeting.meeting_room_type),
                ('id', '!=', meeting.id),
                ('state', '=', 'confirmed'),
            ]
            meetings = self.search_count(domain)
            if meetings:
                raise ValidationError(u'Хурал давхардаж байна')

class HremployeeMeeting(models.Model):
    _name = 'hr.employee.meeting'

    room_id = fields.Many2one('moncon.meeting.room', 'room')
    employee_id = fields.Many2one('hr.employee', 'Ажилтан')



