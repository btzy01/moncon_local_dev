# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class Raises(models.Model):
    _inherit = "raises"

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')
    create_from_regulation = fields.Boolean()
    start_date = fields.Date('Эхлэх огноо')
    end_date = fields.Date('Дуусах огноо')


class HrRegulation(models.Model):
    _inherit = 'hr.regulation'

    type_category = fields.Selection(related='regulation_type_id.category', string=u'Тушаалын утга', readonly=True)
    employee_ids = fields.Many2many('hr.employee', 'hr_regulation_employee_rel', 'regulation_id', 'employee_id', u'Ажилчид')
    employee_lines = fields.One2many('moncon.requlation.line', 'regulation_line_id', string=u'Ажилчид')
    general_end_date = fields.Date('End Date of Implementation')
    compute_employees = fields.Boolean(compute='_compute_employees')
    employee_id = fields.Many2one('hr.employee', string='Ажилтан')
    job_id = fields.Many2one(related='employee_id.job_id', string='Албан тушаал')
    pre_wage = fields.Float(string='Өмнөх үндсэн цалин', readonly=True)
    pre_hour_salary = fields.Float(string='Өмнөх өдрийн цалин', readonly=True)
    pre_kpi_salary = fields.Float(string='Өмнөх хувьсах цалин', readonly=True)
    pre_field_addition = fields.Float(string='Өмнөх талбайн нэмэгдэл', readonly=True)
    pre_country_addition = fields.Float(string='Өмнөх хээрийн нэмэгдэл', readonly=True)
    pre_project_leading_addition = fields.Float(string='Өмнөх ахлахын нэмэгдэл', readonly=True)
    pre_major_addition = fields.Float(string='Өмнөх мэргэшсэн зэргийн нэмэгдэл', readonly=True)
    new_wage = fields.Float(string='Шинэ үндсэн цалин')
    new_hour_salary = fields.Float(string='Шинэ өдрийн цалин')
    new_kpi_salary = fields.Float(string='Шинэ хувьсах цалин')
    new_field_addition = fields.Float(string='Шинэ талбайн нэмэгдэл')
    new_country_addition = fields.Float(string='Шинэ хээрийн нэмэгдэл')
    new_project_leading_addition = fields.Float(string='Шинэ ахлахын нэмэгдэл')
    new_major_addition = fields.Float(string='Шинэ мэргэшсэн зэргийн нэмэгдэл')
    level_pro_raise_ids = fields.One2many('raises', 'regulation_id', string='Нэмэгдэлийн дүн', required=False)
    regulation_type = fields.Selection(string=u'Тушаалын төрөл', related="regulation_type_id.regulation_type")

    #03 Тушаалын талбар
    family_name_3 = fields.Char(string=u'Ургийн овог')
    name_3 = fields.Char(string=u'Нэр')
    last_name_3 = fields.Char(string=u'Овог')
    ssnid_3 = fields.Char(string=u'Регистер')
    identification_id_3 = fields.Char(string=u'Ялгах дугаар')
    mobile_phone_3 = fields.Char(string=u'Ажлын гар утас')
    engagement_in_company_3 = fields.Date(string=u'Ажилд орсон огноо')
    birthday_3 = fields.Date(string=u'Төрсөн огноо')
    work_email_3 = fields.Char(string=u'Ажлын и-мэйл')
    department_id_3 = fields.Many2one('hr.department', string=u'Хэлтэс')
    department_hr_tag_3 = fields.Many2one('department.hr.tag', string=u'Бүтэц')
    job_id_3 = fields.Many2one('hr.job', string=u'Албан тушаал')
    hr_employee_id_3 = fields.Many2one('hr.employee', string=u'ХН ажилтан')
    home_work_space_3 = fields.Char(string=u'Гэрийн хаяг')
    tax_payer_number_3 = fields.Char(string=u'Татвар төлөгчийн дугаар')
    rel_person_name1_3 = fields.Char(string=u'Онцгой шаардлага гарвал харилцах хүний нэр')
    rel_person_phone1_3 = fields.Char(string=u'Онцгой шаардлага гарвал харилцах хүний утас')
    rel_person_name2_3 = fields.Char(string=u'Онцгой шаардлага гарвал харилцах хүний нэр 2')
    rel_person_phone2_3 = fields.Char(string=u'Онцгой шаардлага гарвал харилцах хүний утас 2')
    contract_name_3 = fields.Char(string=u'Гэрээний нэр')
    trial_date_start_3 = fields.Date(string=u'Туршилтын эхлэх огноо')
    trial_date_end_3 = fields.Date(string=u'Туршилтын дуусах огноо')
    new_wage_3 = fields.Float(string=u'Үндсэн цалин')
    new_hour_salary_3 = fields.Float(string=u'Өдрийн цалин')
    new_kpi_salary_3 = fields.Float(string=u'Хувьсах цалин')
    new_field_addition_3 = fields.Float(string=u'Талбайн нэмэгдэл')
    new_country_addition_3 = fields.Float(string=u'Хээрийн нэмэгдэл')
    new_project_leading_addition_3 = fields.Float(string=u'Ахлахын нэмэгдэл')
    new_major_addition_3 = fields.Float(string=u'Мэргэшсэн зэргийн нэмэгдэл')
    new_major_fuel_3 = fields.Float(string=u'Шатахууны нэмэгдэл')
    new_ndsh_type_3 = fields.Many2one('hr.ndsh.config', string=u'НДШ-н төрөл')
    new_working_hours_3 = fields.Many2one('resource.calendar', string=u'Ажлын цагийн хуваарь')
    acc_number_3 = fields.Char(string=u'Дансны дугаар')
    bank_id_3 = fields.Many2one('res.bank', string=u'Банк')


    # 04 Тушаалын талбар
    before_working_hours_4 = fields.Many2one('resource.calendar', string=u'Өмнөх ажлын цагийн хуваарь')
    new_working_hours_4 = fields.Many2one('resource.calendar', string=u'Шинэ ажлын цагийн хуваарь')
    # 08 Тушаалын талбар
    pre_major_addition_8 = fields.Float(string='Өмнөх мэргэшсэн зэргийн нэмэгдэл')
    new_major_addition_8 = fields.Float(string='Шинэ мэргэшсэн зэргийн нэмэгдэл')








    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for obj in self:
            if obj.employee_id and obj.employee_id.contract_id:
                obj.pre_wage = obj.employee_id.contract_id.wage
                obj.pre_hour_salary = obj.employee_id.contract_id.hour_salary
                obj.pre_kpi_salary = obj.employee_id.contract_id.kpi_salary
                obj.pre_field_addition = obj.employee_id.contract_id.field_addition
                obj.pre_country_addition = obj.employee_id.contract_id.country_addition
                obj.pre_project_leading_addition = obj.employee_id.contract_id.project_leading_addition
                obj.pre_major_addition = obj.employee_id.contract_id.major_addition
                obj.before_working_hours_4 = obj.employee_id.contract_id.working_hours
                obj.pre_major_addition_8 = obj.employee_id.contract_id.major_addition


    @api.multi
    def register(self):
        res = super(HrRegulation, self).register()
        for obj in self:
            if obj.regulation_type_id.regulation_type == '21':
                before_wage_str = ''
                wage_str = ''
                if obj.new_wage > 0:
                    obj.employee_id.contract_id.write({'wage': obj.new_wage})
                    before_wage_str += 'Үндсэн цалин: %s, ' % (obj.pre_wage)
                    wage_str += 'Үндсэн цалин: %s, ' % (obj.new_wage)
                if obj.new_hour_salary > 0:
                    obj.employee_id.contract_id.write({'hour_salary': obj.new_hour_salary})
                    before_wage_str += 'Өдрийн цалин: %s, ' % (obj.pre_wage)
                    wage_str += 'Өдрийн цалин: %s, ' % (obj.new_hour_salary)
                if obj.new_kpi_salary > 0:
                    obj.employee_id.contract_id.write({'kpi_salary': obj.new_kpi_salary})
                    before_wage_str += 'Хувьсах цалин: %s, ' % (obj.pre_kpi_salary)
                    wage_str += 'Хувьсах цалин: %s, ' % (obj.new_kpi_salary)
                if obj.new_field_addition > 0:
                    obj.employee_id.contract_id.write({'field_addition': obj.new_field_addition})
                    before_wage_str += 'Талбайн нэмэгдэл: %s, ' % (obj.pre_field_addition)
                    wage_str += 'Талбайн нэмэгдэл: %s, ' % (obj.new_field_addition)
                if obj.new_country_addition > 0:
                    obj.employee_id.contract_id.write({'country_addition': obj.new_country_addition})
                    before_wage_str += 'Хээрийн нэмэгдэл: %s, ' % (obj.pre_country_addition)
                    wage_str += 'Хээрийн нэмэгдэл: %s, ' % (obj.new_country_addition)
                if obj.new_project_leading_addition > 0:
                    obj.employee_id.contract_id.write({'project_leading_addition': obj.new_project_leading_addition})
                    before_wage_str += 'Ахлахын нэмэгдэл: %s, ' % (obj.pre_project_leading_addition)
                    wage_str += 'Ахлахын нэмэгдэл: %s, ' % (obj.new_project_leading_addition)
                if obj.new_major_addition > 0:
                    obj.employee_id.contract_id.write({'major_addition': obj.new_major_addition})
                    before_wage_str += 'Мэргэшсэн зэргийн нэмэгдэл: %s, ' % (obj.pre_major_addition)
                    wage_str += 'Мэргэшсэн зэргийн нэмэгдэл: %s, ' % (obj.new_major_addition)
                if obj.level_pro_raise_ids:
                    for l in obj.level_pro_raise_ids:
                        raises = self.env['raises'].search([('name', '=', l.name.id), ('contract_id', '=', obj.employee_id.contract_id.id)])
                        raises.unlink()
                        obj.employee_id.contract_id.update(
                            {'level_pro_raise_ids': [(0, 0, {
                                'name': l.name,
                                'type': l.type,
                                'raise_size': l.raise_size,
                                'period_id': l.period_id.id,
                                'repeatedly': l.repeatedly,
                                'repeatedly_count': l.repeatedly_count,
                                'contract_id': obj.employee_id.contract_id.id,
                                'create_from_regulation': obj.reg_number,
                            })]}
                        )
                self.env['hr.contract.wage.history'].create({
                    'regulation_id': obj.id,
                    'contract_id': obj.employee_id.contract_id.id,
                    'before_wage': before_wage_str,
                    'wage': wage_str,
                    'start_date': obj.general_start_date,
                    'date': fields.Date.today(),
                    'user_id': self.env.uid,
                    'regulation_type_name': obj.regulation_type_id.name,
                })
            if obj.regulation_type_id.regulation_type == '29':
                if obj.employee_lines:
                    for line in obj.employee_lines:
                        if not line.employee_id.contract_id.id:
                            raise ValidationError(_("%s албан тушаалтай %s-н гэрээ байхгүй байна!!!") %(line.job_id.name, line.name))
                        elif line.employee_id.contract_id.id:
                            before_salary = line.employee_id.contract_id.worked_year_addition
                            line.employee_id.contract_id.update({
                                'worked_year_addition': line.worked_year_add,})
                        self.env['hr.contract.wage.history'].create({
                            'regulation_id': obj.id,
                            'contract_id': line.employee_id.contract_id.id,
                            'before_wage': before_salary,
                            'wage': line.worked_year_add,
                            'start_date': obj.general_start_date,
                            'date': fields.Date.today(),
                            'user_id': self.env.uid,
                            'regulation_type_name': obj.regulation_type_id.name,
                        })
            if obj.regulation_type_id.regulation_type == '3':
                new_employee = self.env['hr.employee'].create({
                    'identification_id': obj.identification_id_3,
                    'famliy_name': obj.family_name_3,
                    'name': obj.name_3,
                    'last_name': obj.last_name_3,
                    'ssnid': obj.ssnid_3,
                    'taxpayer_number': obj.tax_payer_number_3,
                    'mobile_phone': obj.mobile_phone_3,
                    'home_work_space': obj.home_work_space_3,
                    'engagement_in_company': obj.engagement_in_company_3,
                    'birthday': obj.birthday_3,
                    'work_email': obj.work_email_3,
                    'department_id': obj.department_id_3.id,
                    'department_hr_tag_ids': obj.department_hr_tag_3.id,
                    'job_id': obj.job_id_3.id,
                    'hr_employee_id': obj.hr_employee_id_3.id,
                    'rel_person_name': obj.rel_person_name1_3,
                    'rel_person_phone': obj.rel_person_phone1_3,
                    'rel_person_name2': obj.rel_person_name2_3,
                    'rel_person_phone2': obj.rel_person_phone2_3,
                    'state_id': 2,})
                self.env['hr.contract'].create({
                    'employee_id': new_employee.id,
                    'name': obj.contract_name_3,
                    'trial_date_start': obj.trial_date_start_3,
                    'trial_date_end': obj.trial_date_end_3,
                    'wage': obj.new_wage_3,
                    'hour_salary': obj.new_hour_salary_3,
                    'kpi_salary': obj.new_kpi_salary_3,
                    'field_addition': obj.new_field_addition_3,
                    'country_addition': obj.new_country_addition_3,
                    'project_leading_addition': obj.new_project_leading_addition_3,
                    'major_addition': obj.new_major_addition_3,
                    'fuel_addition': obj.new_major_fuel_3,
                    'ndsh_type': obj.new_ndsh_type_3.id,
                    'state': 'open',
                    'working_hours': obj.new_working_hours_3.id,})
                bank_account = self.env['res.partner.bank'].create({
                    'acc_number': obj.acc_number_3,
                    'bank_id': obj.bank_id_3.id,
                    'partner_id': new_employee.address_home_id.id, })
                new_employee.update({'bank_account_id': bank_account.id,})
            if obj.regulation_type_id.regulation_type == '4':
                obj.employee_id.contract_id.write({'working_hours': obj.new_working_hours_4.id})
                self.env['working.hours.change.history'].create({
                    'regulation_id': obj.id,
                    'contract_id': obj.employee_id.contract_id.id,
                    'before_working_hours_id': obj.before_working_hours_4.id,
                    'working_hours_id': obj.new_working_hours_4.id,
                    'edit_date': obj.general_start_date,
                    'date': fields.Date.today(),
                    'user_id': self.env.uid,
                    'regulation_type_name': obj.regulation_type_id.name,
                })
            if obj.regulation_type_id.regulation_type == '8':
                obj.employee_id.contract_id.write({'major_addition': obj.new_major_addition_8})
                self.env['hr.contract.wage.history'].create({
                    'regulation_id': obj.id,
                    'contract_id': obj.employee_id.contract_id.id,
                    'before_wage': obj.pre_major_addition_8,
                    'wage': obj.new_major_addition_8,
                    'start_date': obj.general_start_date,
                    'date': fields.Date.today(),
                    'user_id': self.env.uid,
                    'regulation_type_name': obj.regulation_type_id.name,
                })


        return res

    @api.multi
    @api.onchange('employee_ids', 'employee_lines')
    def _compute_employees(self):
        for obj in self:
            if not self.employee_ids and self.employees:
                obj.write({'employee_ids': [[6, False, self.employees.mapped('employee').ids]]})
            obj.compute_employees = False

    @api.model
    def create(self, vals):
        if 'employee_id' in vals:
            employee_obj = self.env['hr.employee'].browse(vals['employee_id'])
            vals['pre_wage'] = employee_obj.contract_id.wage if employee_obj.contract_id else None
            vals['pre_hour_salary'] = employee_obj.contract_id.hour_salary if employee_obj.contract_id else None
            vals['before_working_hours_4'] = employee_obj.contract_id.working_hours.id if employee_obj.contract_id else None
            vals['pre_kpi_salary'] = employee_obj.contract_id.kpi_salary if employee_obj.contract_id else None
            vals['pre_field_addition'] = employee_obj.contract_id.field_addition if employee_obj.contract_id else None
            vals['pre_country_addition'] = employee_obj.contract_id.country_addition if employee_obj.contract_id else None
            vals['pre_project_leading_addition'] = employee_obj.contract_id.project_leading_addition if employee_obj.contract_id else None
            vals['pre_major_addition'] = employee_obj.contract_id.major_addition if employee_obj.contract_id else None
            vals['pre_major_addition_8'] = employee_obj.contract_id.major_addition if employee_obj.contract_id else None
        if vals.get('employee_ids'):
            vals['employees'] = []
            for obj in vals['employee_ids'][0][2]:
                vals['employees'].append([0, False, {'employee': obj}])
        res = super(HrRegulation, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if 'employee_id' in vals:
            employee_obj = self.env['hr.employee'].browse(vals['employee_id'])
            vals['pre_wage'] = employee_obj.contract_id.wage if employee_obj.contract_id else None
            vals['pre_hour_salary'] = employee_obj.contract_id.hour_salary if employee_obj.contract_id else None
            vals['pre_kpi_salary'] = employee_obj.contract_id.kpi_salary if employee_obj.contract_id else None
            vals['before_working_hours_4'] = employee_obj.contract_id.working_hours.id if employee_obj.contract_id else None
            vals['pre_field_addition'] = employee_obj.contract_id.field_addition if employee_obj.contract_id else None
            vals['pre_country_addition'] = employee_obj.contract_id.country_addition if employee_obj.contract_id else None
            vals['pre_project_leading_addition'] = employee_obj.contract_id.project_leading_addition if employee_obj.contract_id else None
            vals['pre_major_addition'] = employee_obj.contract_id.major_addition if employee_obj.contract_id else None
            vals['pre_major_addition_8'] = employee_obj.contract_id.major_addition if employee_obj.contract_id else None
        res = super(HrRegulation, self).write(vals)
        for obj in self:
            obj.employees.filtered(lambda l: l.employee not in obj.employee_ids).unlink()
        return res

    @api.one
    def action_to_confirm(self):
        self.send_notification()
        if self.regulation_type_id.category == 'b':
            self.write({'reg_number': self.env['ir.sequence'].next_by_code('hr.regulation')})
        else:
            self.write({'reg_number': self.env['ir.sequence'].next_by_code('hr.regulation.a')})
        return self.write({'state': 'confirmed'})

class HrRegulationEmployee(models.Model):
    _inherit = 'hr.regulation.employee'

    type_category = fields.Selection(related='regulation.type_category', string=u'Тушаалын утга', readonly=True, store=True)

    @api.constrains('start_date', 'end_date')
    def check_start_end_date(self):
        for obj in self:
            if obj.regulation and obj.regulation.general_start_date and obj.regulation.general_start_date > obj.start_date:
                raise ValidationError(_("%s Employee regulation start date must be after %s ") %
                                      (obj.employee.name, obj.regulation.general_start_date))
            if obj.regulation and obj.regulation.general_end_date and obj.regulation.general_end_date < obj.end_date:
                raise ValidationError(_("%s Employee regulation end date must be before %s ") %
                                      (obj.employee.name, obj.regulation.general_end_date))

    @api.model
    def create(self, vals):
        if vals.get('regulation'):
            regulation = self.env['hr.regulation'].browse(vals['regulation'])
            if regulation.general_start_date and not vals.get('start_date'):
                vals['start_date'] = regulation.general_start_date
            if regulation.general_end_date and not vals.get('end_date'):
                vals['end_date'] = regulation.general_end_date
        return super(HrRegulationEmployee, self).create(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.regulation:
                query = '''
                    DELETE FROM hr_regulation_employee_rel
                    WHERE regulation_id = %s and employee_id = %s
                ''' % (obj.regulation.id, obj.employee.id)
                self.env.cr.execute(query)
        return super(HrRegulationEmployee, self).unlink()


class HrRegulationType(models.Model):
    _inherit = 'hr.regulation.type'


    regulation_type = fields.Selection([
        ('1', u'Ажил түр орлон гүйцэтгүүлэх тухай'),
        ('2', u'Ажил хавсран гүйцэтгэх тухай'),
        ('3', u'Ажилд томилох тухай'),
        ('4', u'Ажлын цагийн хуваарьт өөрчлөлт оруулах тухай'),
        ('5', u'Ажлын цагт өөрчлөлт оруулах тухай'),
        ('6', u'Жирэмсний болон амаржсаны амралт, хүүхэд асрах чөлөө олгох'),
        ('7', u'Жирэмсний болон амаржсаны, хүүхэд асрах чөлөө олгох тухай'),
        ('8', u'Зэргийн нэмэгдэл олгох тухай'),
        ('9', u'Нөхөн амрах хоног тооцож олгох тухай'),
        ('10', u'Сахилгын шийтгэл ногдуулах тухай'),
        ('11', u'Тогтвортой ажилласан жилийн нэмэгдэл нөхөн олгох тухай'),
        ('12', u'Төсөлд шилжүүлэн томилох тухай'),
        ('13', u'Туршилтын хугацаа сунгах тухай'),
        ('14', u'Түлшний лимит тооцож олгох тухай'),
        ('15', u'Тэтгэмж олгох тухай'),
        ('16', u'Тэтгэмж, чөлөө олгох тухай'),
        ('17', u'Урт хугацааны чөлөө олго'),
        ('18', u'Урт хугацааны чөлөө олгох тухай'),
        ('19', u'Утасны лимитэд өөрчлөлт оруулах тухай'),
        ('20', u'Үндсэн ажилтнаар томилох тухай'),
        ('21', u'Үндсэн цалинд өөрчлөлт оруулах тухай'),
        ('22', u'Хөдөлмөрийн гэрээ дуусгавар болгож, ажилд томилох тухайй'),
        ('23', u'Хөдөлмөрийн гэрээ дуусгавар болгох тухай'),
        ('24', u'Хөдөлмөрийн гэрээ цуцлах тухай'),
        ('25', u'Хөдөлмөрийн гэрээг дуусгавар болгож, ажилд томилох тухай'),
        ('26', u'Хүүхэд асрах нэмэлт завсарлага олгох тухай'),
        ('27', u'Чөлөө олгох тухай'),
        ('28', u'Ээлжийн амралтын цалин тооцох тухай'),
        ('29', u'Тогтвортой ажилласан жилийн нэмэгдэл олгох тухай'),
        ('30', u'Шагналын төсөв батлах тухай'),], string=u'Тушаалын төрөл')


class MonconRegulationLine(models.Model):
    _name = 'moncon.requlation.line'
    _rec_name = 'employee_id'



    @api.onchange('employee_id')
    def onchange_employee(self):
        for obj in self:
            if obj.pre_worked_year_add == 0 and obj.employee_id.contract_id:
                obj.pre_worked_year_add = obj.employee_id.contract_id.worked_year_addition

    regulation_line_id = fields.Many2one('hr.regulation', ondelete='cascade')
    employee_id = fields.Many2one('hr.employee', string=u'Ажилтан', required=True, index=True)
    ssnid = fields.Char(related="employee_id.ssnid", string=u'Регистр')
    last_name = fields.Char(related="employee_id.last_name", string=u'Овог')
    name = fields.Char(related="employee_id.name", string=u'Нэр')
    engagement_in_company = fields.Date(related="employee_id.engagement_in_company", string=u'Ажилд орсон огноо')
    pre_worked_year_add = fields.Float(string=u'Өмнөх дүн')
    worked_year_add = fields.Float(string=u'Нэмэгдэл тооцож олгох дүн')
    duration_of_emp_details = fields.Char(related="employee_id.duration_of_emp_details", string=u'Ажилласан жил')
    company_id = fields.Many2one('res.company', string=u'Компани', default=lambda self: self.env.user.company_id)



    @api.model
    def create(self, vals):
        if 'employee_id' in vals:
            employee_obj = self.env['hr.employee'].browse(vals['employee_id'])
            vals['pre_worked_year_add'] = employee_obj.contract_id.worked_year_addition if self.pre_worked_year_add == 0 and employee_obj.contract_id else None
        res = super(MonconRegulationLine, self).create(vals)
        return res



