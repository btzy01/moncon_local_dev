# -*- coding: utf-8 -*-
# Part of Oderp10. See LICENSE file for full copyright and licensing details.
{
    'name': "Moncon Human Resource Regulation",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr_regulation',
        'l10n_mn_moncon_hr_contract',
        'l10n_mn_hr_payroll',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         Moncon HR Regulation custom made for Moncon
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/hr_regulation.xml',
        'views/sequence_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
