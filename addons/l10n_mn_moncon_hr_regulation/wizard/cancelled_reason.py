# -*- coding: utf-8 -*-

from odoo import _, api, fields, models


class RegulationCancelledReason(models.TransientModel):
    _inherit = 'regulation.cancelled.reason'

    def cancelled_reason(self):
        res = super(RegulationCancelledReason, self).cancelled_reason()
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        if context.get('active_model') == 'hr.regulation' and context.get('active_ids'):
            for line in self.env['hr.regulation'].browse(active_ids):
                if line.regulation_type_id.regulation_type == '21' and line.state == 'refuse':
                    if line.employee_id and line.employee_id.contract_id:
                        line.employee_id.contract_id.write({
                            'wage': line.pre_wage,
                            'hour_salary': line.pre_hour_salary,
                            'kpi_salary': line.pre_kpi_salary,
                            'field_addition': line.pre_field_addition,
                            'country_addition': line.pre_country_addition,
                            'project_leading_addition': line.pre_project_leading_addition,
                            'major_addition': line.pre_major_addition,
                        })
                        history = self.env['hr.contract.wage.history'].search([('regulation_id', '=', line.id)], limit=1)
                        if history:
                            history.unlink()
                        raises = self.env['raises'].search([('contract_id', '=', line.employee_id.contract_id.id), ('create_from_regulation', '=', line.reg_number)], limit=1)
                        if raises:
                            raises.sudo().unlink()
                elif line.regulation_type_id.regulation_type == '29' and line.state == 'refuse':
                    history_lines = self.env['hr.contract.wage.history'].search([('regulation_id', '=', line.id)],)
                    for reg_line in line.employee_lines:
                        for obj in history_lines:
                            if obj.contract_id.id == reg_line.employee_id.contract_id.id:
                                reg_line.employee_id.contract_id.write({
                                    'worked_year_addition': obj.before_wage,
                                })
                                self._cr.execute("DELETE FROM hr_contract_wage_history WHERE id = %s", (obj.id,))
                elif line.regulation_type_id.regulation_type == '3' and line.state == 'refuse':
                    created_employee = self.env['hr.employee'].search([('ssnid', '=', line.ssnid_3)],)
                    created_employee_partner = self.env['res.partner'].search([('register', '=', line.ssnid_3)],)
                    created_employee_contract = self.env['hr.contract'].search([('identification_id', '=', line.ssnid_3)],)
                    if len(created_employee) == 1:
                        self._cr.execute("DELETE FROM res_partner_bank WHERE partner_id = %s", (created_employee_partner.id,))
                        self._cr.execute("DELETE FROM hr_contract WHERE id = %s", (created_employee_contract.id,))
                        self._cr.execute("DELETE FROM res_partner WHERE id = %s", (created_employee_partner.id,))
                        self._cr.execute("DELETE FROM hr_employee WHERE id = %s", (created_employee.id,))
                    else:
                        return False
                elif line.regulation_type_id.regulation_type == '4' and line.state == 'refuse':
                    if line.employee_id and line.employee_id.contract_id:
                        line.employee_id.contract_id.write({
                            'working_hours': line.before_working_hours_4.id,
                        })
                        history = self.env['working.hours.change.history'].search([('regulation_id', '=', line.id)], limit=1)
                        self._cr.execute("DELETE FROM working_hours_change_history WHERE id = %s", (history.id,))
                    else:
                        return False
                elif line.regulation_type_id.regulation_type == '8' and line.state == 'refuse':
                    if line.employee_id and line.employee_id.contract_id:
                        line.employee_id.contract_id.write({
                            'major_addition': line.pre_major_addition_8,
                        })
                        history = self.env['hr.contract.wage.history'].search([('regulation_id', '=', line.id)], limit=1)
                        self._cr.execute("DELETE FROM hr_contract_wage_history WHERE id = %s", (history.id,))
                    else:
                        return False


        return res
