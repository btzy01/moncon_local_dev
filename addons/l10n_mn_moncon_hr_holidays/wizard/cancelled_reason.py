# -*- coding: utf-8 -*-
import pytz

from datetime import date, datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class CancelledReason(models.TransientModel):
    _name = 'cancelled.reason'
    _description = u'Цуцлах тайлбар'

    reason = fields.Char('Тайлбар', required=True)

    def cancelled_reason(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        tz = get_user_timezone(self.env.user)
        str_today = str(datetime.today().strftime('%Y-%m-%d'))
        today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d')).astimezone(tz)
        if context.get('active_model') == 'moncon.hr.holidays' and context.get('active_ids'):
            for line in self.env['moncon.hr.holidays'].browse(active_ids):
                if line.type == 'add' and line.state == 'approved':
                    line.employee_id.holiday_day_count -= line.number_of_hours_temp
                if line.type == 'remove' and line.state == 'approved':
                    line.employee_id.holiday_day_count += line.number_of_hours_temp
                if line.type == 'money' and line.state == 'approved':
                    line.employee_id.holiday_day_count += line.number_of_hours_temp
                if line.workflow_id:
                    success = line.env['workflow.config'].reject('moncon.hr.holidays.workflow.history', 'history', line, line.env.user.id)
                    if success:
                        line.cancelled_reason = self.reason
                        line.write({'state': 'refuse', 'cancel_date': today + timedelta(days=7)})
                line.write({'state': 'refuse', 'cancel_date': today + timedelta(days=7)})
        elif context.get('active_model') == 'hr.holidays' and context.get('active_ids'):
            for line in self.env['hr.holidays'].browse(active_ids):
                if line.workflow_id:
                    success = line.env['workflow.config'].reject('hr.holidays.workflow.history', 'history', line, line.env.user.id)
                    if success:
                        line.cancelled_reason = self.reason
                        line.write({'state': 'refuse'})
                line.write({'state': 'refuse'})
        else:
            return False
