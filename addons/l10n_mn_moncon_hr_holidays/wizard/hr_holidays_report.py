# -*- coding: utf-8 -*-

import base64
from io import BytesIO
from lxml import etree
import time
import xlsxwriter
import xlwt
from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.models.report_helper import *
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import AccessError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from StringIO import StringIO


class HRHolidaysReport(models.TransientModel):
    """
        Нөхөн амралтын тайлан
    """
    _name = 'hr.holidays.report'

    @api.model
    def get_default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, ondelete='cascade', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Date From', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date('Date To', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    employee_ids = fields.Many2many('hr.employee', 'hr_holidays_report_employee_rel', 'wizard_id', 'employee_id', string='Employee', required=True, ondelete='cascade', default=get_default_employee)

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = u'Нөхөж амрах хоногийн бүртгэл'
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_number = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        seq = 1
        data_dict = {}
        rowx = 0
        catogeries = []
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('hr_holidays_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 30)


        sheet.merge_range(rowx, 0, rowx, 1, '%s' % (self.company_id.name), format_filter)
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 4, report_name, format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Print Date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 1

        sheet.merge_range(rowx, 0, rowx + 2, 0, u'№', format_title)
        sheet.merge_range(rowx, 1, rowx + 2, 1, u'Регистрын дугаар', format_title)
        sheet.merge_range(rowx, 2, rowx + 2, 2, u'Овог', format_title)
        sheet.merge_range(rowx, 3, rowx + 2, 3, u'Нэр', format_title)
        sheet.merge_range(rowx, 4, rowx + 2, 4, u'Албан тушаал', format_title)
        sheet.merge_range(rowx, 5, rowx + 1, 5, u'1', format_title)
        sheet.merge_range(rowx, 6, rowx + 1, 6, u'1', format_title)
        sheet.write(rowx + 2, 5, u'+', format_title)
        sheet.write(rowx + 2, 6, u'-', format_title)
        sheet.merge_range(rowx, 7, rowx + 1, 7, u'2', format_title)
        sheet.merge_range(rowx, 8, rowx + 1, 8, u'2', format_title)
        sheet.write(rowx + 2, 7, u'+', format_title)
        sheet.write(rowx + 2, 8, u'-', format_title)
        sheet.merge_range(rowx, 9, rowx + 1, 9, u'3', format_title)
        sheet.merge_range(rowx, 10, rowx + 1, 10, u'3', format_title)
        sheet.write(rowx + 2, 9, u'+', format_title)
        sheet.write(rowx + 2, 10, u'-', format_title)
        sheet.merge_range(rowx, 11, rowx + 1, 11, u'4', format_title)
        sheet.merge_range(rowx, 12, rowx + 1, 12, u'4', format_title)
        sheet.write(rowx + 2, 11, u'+', format_title)
        sheet.write(rowx + 2, 12, u'-', format_title)
        sheet.merge_range(rowx, 13, rowx + 1, 13, u'5', format_title)
        sheet.merge_range(rowx, 14, rowx + 1, 14, u'5', format_title)
        sheet.write(rowx + 2, 13, u'+', format_title)
        sheet.write(rowx + 2, 14, u'-', format_title)
        sheet.merge_range(rowx, 15, rowx + 1, 15, u'6', format_title)
        sheet.merge_range(rowx, 16, rowx + 1, 16, u'6', format_title)
        sheet.write(rowx + 2, 15, u'+', format_title)
        sheet.write(rowx + 2, 16, u'-', format_title)
        sheet.merge_range(rowx, 17, rowx + 1, 17, u'7', format_title)
        sheet.merge_range(rowx, 18, rowx + 1, 18, u'7', format_title)
        sheet.write(rowx + 2, 17, u'+', format_title)
        sheet.write(rowx + 2, 18, u'-', format_title)
        sheet.merge_range(rowx, 19, rowx + 1, 19, u'8', format_title)
        sheet.merge_range(rowx, 20, rowx + 1, 20, u'8', format_title)
        sheet.write(rowx + 2, 19, u'+', format_title)
        sheet.write(rowx + 2, 20, u'-', format_title)
        sheet.merge_range(rowx, 21, rowx + 1, 21, u'9', format_title)
        sheet.merge_range(rowx, 22, rowx + 1, 22, u'9', format_title)
        sheet.write(rowx + 2, 21, u'+', format_title)
        sheet.write(rowx + 2, 22, u'-', format_title)
        sheet.merge_range(rowx, 23, rowx + 1, 23, u'10', format_title)
        sheet.merge_range(rowx, 24, rowx + 1, 24, u'10', format_title)
        sheet.write(rowx + 2, 23, u'+', format_title)
        sheet.write(rowx + 2, 24, u'-', format_title)
        sheet.merge_range(rowx, 25, rowx + 1, 25, u'11', format_title)
        sheet.merge_range(rowx, 26, rowx + 1, 26, u'11', format_title)
        sheet.write(rowx + 2, 25, u'+', format_title)
        sheet.write(rowx + 2, 26, u'-', format_title)
        sheet.merge_range(rowx, 27, rowx + 1, 27, u'12', format_title)
        sheet.merge_range(rowx, 28, rowx + 1, 28, u'12', format_title)
        sheet.write(rowx + 2, 27, u'+', format_title)
        sheet.write(rowx + 2, 28, u'-', format_title)

        sheet.merge_range(rowx, 29, rowx + 2, 29, u'Өмнөх оны үлдэгдэл цаг', format_title)
        sheet.merge_range(rowx, 30, rowx + 2, 30, u'Өмнөх оны үлдэгдэл хоног', format_title)
        sheet.merge_range(rowx, 31, rowx + 2, 31, u'Нийт үлдэгдэл цаг', format_title)
        sheet.merge_range(rowx, 32, rowx + 2, 32, u'Нийт үлдэгдэл хоног', format_title)





        rowx += 2

        #             name_selection_groups
        # create date
        rowx += 10
        user = self.env['res.users'].browse(self._uid)
        sheet.write(rowx, 0, '%s: ........................................... /%s/' % (u'Owner', user.name), format_filter)
        book.close()

        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

