# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - holidays module',
    'version': '1.0',
    'author': 'Moncon LLC',
    "description": """
     Уг модуль нь Монконы ерөнхий Амралт чөлөөний модуль болно.
 """,
    'website': 'http://www.moncon.erp.mn',
    'images': [''],
    'depends': ['l10n_mn_hr_holidays', 'l10n_mn_hr_hour_balance', 'hr_holidays', 'web_readonly_bypass'],
    'data': [
        'security/ir.model.access.csv',

        'wizard/cancelled_reason_view.xml',
        'views/hr_holidays_view.xml',
        'views/hr_holidays_type_view.xml',
        'views/moncon_hr_holidays_view.xml',
        'views/hr_emplpyee_view.xml',
        'wizard/hr_holidays_report_view.xml',
        'security/hr_holidays_security.xml',
        'views/hr_contract_view.xml',
        'report/compensation_detail_report_view.xml'

    ],
    'installable': True,
    'auto_install': False
}
