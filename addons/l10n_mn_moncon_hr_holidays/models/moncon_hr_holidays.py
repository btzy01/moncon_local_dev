# -*- coding: utf-8 -*-

from datetime import date, datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError
from odoo import exceptions
import calendar
from odoo import tools
from datetime import timedelta, datetime
import pytz

class MonconHrHolidays(models.Model):
    _name = "moncon.hr.holidays"
    _inherit = 'mail.thread'

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    @api.depends('type')
    @api.onchange('type')
    def _compute_total_holidays_days(self):
        for record in self:
            if record.employee_id.holiday_day_count:
                moncon_hr_holidays = self.env['moncon.hr.holidays'].sudo().search(
                    [('employee_id', '=', record.employee_id.id), ('state', '=', 'send'), ('type', '=', 'remove')])
                total_holiday_day = 0.0
                for line in moncon_hr_holidays:
                    total_holiday_day += line.number_of_hours_temp
                total_holiday_day = record.employee_id.holiday_day_count - total_holiday_day
                record.employee_holidays_days = total_holiday_day

    name = fields.Char(u'Нэр')
    type = fields.Selection([('money', u'Нөхөн амрах хоног илүү цагаар тооцуулах'),
                             ('add', u'Нөхөн амрах хоног хуримтлуулах'),
                             ('remove', u'Нөхөн амрах хоног биеэр эдлэх')], default='add')
    date_from = fields.Datetime(u'Эхлэх өдөр')
    date_to = fields.Datetime(u'Дуусах өдөр')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Хүсэлт илгээгдсэн'),
                              ('approved', u'Зөвшөөрсөн'),
                              ('refuse', u'Хүсэлт цуцлагдсан'),
                              ], 'Төлөв', default='draft', track_visibility='onchange')
    comment = fields.Char(u'Тайлбар')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')
    employee_id = fields.Many2one('hr.employee', string='Ажилтан', index=True, readonly=True,
                                  states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]}, default=_default_employee)
    department_id = fields.Many2one('hr.department', related='employee_id.department_id', string='Хэлтэс', readonly=True, store=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_workflow_history = fields.Boolean(compute='_is_workflow_history')
    number_of_hours_temp = fields.Float('Allocation', readonly=True, copy=False,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    number_of_days_temp = fields.Float('Allocation', readonly=True, copy=False,
                                       states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    notes = fields.Text('Notes')
    history_lines = fields.One2many('moncon.hr.holidays.workflow.history', 'history', 'Workflow History')
    cancelled_reason = fields.Char('Цуцалсан тайлбар', readonly=True)
    hr_employee_id = fields.Many2one('hr.employee', related = 'employee_id.hr_employee_id', string='ХН ажилтан',store=True, readonly=True,)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    show_hr_button = fields.Boolean(string='Show Cancel Approve Button?', compute='_show_approve_button')
    total_moncon_holiday = fields.Float(u'Тухайн сарын нийт нөхөн амарсан цаг', compute='_compute_total_moncon_holiday' )
    add_totol_moncon_hr_holidays = fields.Float(u'Тухайн сарын нийт нөхөн амралт хуримтлуулсан цаг', compute='_compute_total_moncon_holiday' )
    employee_holidays_days = fields.Float(compute='_compute_total_holidays_days', store=True)
    cancel_date = fields.Date()
    total_moncon_holiday_limit = fields.Float(related='employee_id.contract_id.total_moncon_holiday_limit', string='Нөхөн амрах илүү цагийн хязгаар')

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_DEPARTMENT':
                        args[index] = (args[index][0], args[index][1], user.allowed_department_ids.ids)
        return super(MonconHrHolidays, self).search(args, offset=offset, limit=limit, order=order, count=count)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_DEPARTMENT':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_department_ids.ids)
        return super(MonconHrHolidays, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)


    def _compute_total_moncon_holiday(self):
        for record in self:
            if record.date_from:
                date_start = datetime.strptime(record.date_from, "%Y-%m-%d %H:%M:%S")
                month = date_start.month
                year = date_start.year
                res = calendar.monthrange(date_start.year, date_start.month)[1]
                date_from = (datetime(year, month, 1,8,0,0))
                date_to = (datetime(year, month, res,15,59,59))
                start = get_display_day_to_user_day(date_from, self.env.user).strftime('%Y%m%d_%H%M')
                stop = get_day_like_display(date_to, self.env.user).strftime('%Y%m%d_%H%M')
                moncon_hr_holidays = self.env['moncon.hr.holidays'].sudo().search(
                    [('employee_id', '=', record.employee_id.id), ('date_from', '>=', start),
                    ('date_to', '<=', stop), ('state', '=', 'approved'), ('type', '=', 'remove')])
                total_day_hour = 0.0
                for line in moncon_hr_holidays:
                    total_day_hour += line.number_of_hours_temp
                add_totol_moncon_hr_holidays = self.env['moncon.hr.holidays'].sudo().search(
                    [('employee_id', '=', record.employee_id.id), ('date_from', '>=', start),
                    ('date_to', '<=', stop), ('state', '=', 'approved'), ('type', '=', 'add')])
                total_add_day_hour = 0.0
                for add_line in add_totol_moncon_hr_holidays:
                    total_add_day_hour += add_line.number_of_hours_temp
                record.add_totol_moncon_hr_holidays = total_add_day_hour
                record.total_moncon_holiday = total_day_hour

    @api.multi
    def _show_approve_button(self):
        for obj in self:
            if obj.state == 'send' and obj.type == 'money' and obj.env.user.has_group("l10n_mn_moncon_hr_holidays.group_hr_holiday_edit"):
                obj.show_hr_button = True
            else:
                obj.show_hr_button = False

    @api.constrains('date_from', 'date_to')
    def _check_date(self):
        for holiday in self:
            domain = [
                ('date_from', '<=', holiday.date_to),
                ('date_to', '>=', holiday.date_from),
                ('employee_id', '=', holiday.employee_id.id),
                ('id', '!=', holiday.id),
                ('type', '=', holiday.type),
                ('state', '!=', 'refuse'),
            ]
            nholidays = self.search_count(domain)
            if nholidays:
                raise ValidationError(_('You can not have 2 leaves that overlaps on same day!'))

    @api.onchange('date_from', 'type')
    def _onchange_add_date_from(self):
        date_from = self.date_from
        date_to = self.date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.type == 'add' or self.type == 'money':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from,
                                                                                                     self.env.user).hour <= 12 and get_day_like_display(date_to,
                                                                                                                                                        self.env.user).hour >= 15 and get_day_like_display(
                        date_to, self.env.user).hour <= 21:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 - 1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.type == 'add' or self.type == 'money':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8
            else:
                if (date_to and date_from) and (date_from <= date_to):
                    self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
                    self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
                else:
                    self.number_of_days_temp = 0
                    self.number_of_hours_temp = 0

    @api.model
    def create(self, vals):
        creation = super(MonconHrHolidays, self).create(vals)

        if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days') and vals['date_from']:
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(vals['date_from'], '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            if today.weekday() == 0:
                str_start = today - timedelta(days=7)
            else:
                str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date() and (vals['type'] != 'money'):
                raise UserError(u'Тухайн долоо хоногийн нөхөн амралт хүсэлтийн хугацаа хэтэрсэн байна!!!')

        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('tag', 'hr.holidays', employee.id)
        workflow = None
        if workflow_id:
            workflow = workflow_id
        if not workflow:
            raise exceptions.Warning('There is no workflow defined!')
        creation.workflow_id = workflow
        if 'date_to' in vals and 'type' in vals:
            if vals['type'] == 'remove':
                d_now = get_day_like_display(vals['date_to'], self.env.user)
                d_to = d_now.replace(hour=11, minute=0, second=0)
                if d_now <= d_to:
                    raise UserError('Биеэр эдлэх чөлөөг 11-н цагаас хойш үүсгэх боломжтой.')
        return creation

    @api.multi
    def write(self, vals):
        if 'type' in vals and 'date_from' not in vals:
            if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days') and self.date_from:
                tz = get_user_timezone(self.env.user)
                date_from = pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
                today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                if today.weekday() == 0:
                    str_start = today - timedelta(days=7)
                else:
                    str_start = today - timedelta(today.weekday())
                if self.cancel_date:
                    if datetime.strptime(self.cancel_date, '%Y-%m-%d').date() < date_from.date():
                        raise UserError(u'7 хоног хэтэрсэн байна.')
                else:
                    if str_start.date() > date_from.date() and (vals['type'] != 'money'):
                        raise UserError(u'Тухайн долоо хоногийн нөхөн амралт хүсэлтийн хугацаа хэтэрсэн байна!!!')
                # day = 0
                # if today.weekday() in (0,6):
                #     day = 6
                # elif today.weekday() in (1,5):
                #     day = 5
                # elif today.weekday() in (3,4):
                #     day = 4
                # elif today.weekday() == 2:
                #     day = 3
                # str_start = today - timedelta(days=day)
                # if str_start.date() >= date_from.date() and (vals['type'] != 'money'):
                #     raise UserError(u'Тухайн нөхөн амралтын 72 цагийн лимит хэтэрсэн байна!!!')
        elif 'type' not in vals and 'date_from' in vals:
            if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days') and 'date_from' in vals:
                tz = get_user_timezone(self.env.user)
                date_from = pytz.utc.localize(datetime.strptime(vals['date_from'], '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
                today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                if today.weekday() == 0:
                    str_start = today - timedelta(days=7)
                else:
                    str_start = today - timedelta(today.weekday())
                if self.cancel_date:
                    if datetime.strptime(self.cancel_date, '%Y-%m-%d').date() < date_from.date():
                        raise UserError(u'7 хоног хэтэрсэн байна.')
                else:
                    if str_start.date() >= date_from.date() and self.type != 'money':
                        raise UserError(u'Тухайн долоо хоногийн нөхөн амралт хүсэлтийн хугацаа хэтэрсэн байна!!!')
        if 'date_to' in vals and 'type' not in vals:
            if self.type == 'remove':
                s_now = get_day_like_display(vals['date_to'], self.env.user)
                s_to = s_now.replace(hour=11, minute=0, second=0)
                if s_now <= s_to:
                    raise UserError('Биеэр эдлэх чөлөөг 11-н цагаас хойш үүсгэх боломжтой.')
        if 'type' in vals:
            if vals['type'] == 'remove':
                s_now = get_day_like_display(self.date_to, self.env.user)
                s_to = s_now.replace(hour=11, minute=0, second=0)
                if s_now <= s_to and 'date_to' not in vals:
                    raise UserError('Биеэр эдлэх чөлөөг 11-н цагаас хойш үүсгэх боломжтой.')
                v_now = get_day_like_display(vals['date_to'], self.env.user)
                v_to = s_now.replace(hour=11, minute=0, second=0)
                if v_now <= v_to:
                    raise UserError('Биеэр эдлэх чөлөөг 11-н цагаас хойш үүсгэх боломжтой.')
        return super(MonconHrHolidays, self).write(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError('Зөвхөн ноорог төлөвтэй үед устгах боломжтой')
        return super(MonconHrHolidays, self).unlink()

    @api.multi
    def action_send(self):
        if self.filtered(lambda holiday: holiday.state != 'draft'):
            raise UserError(_('Leave request must be in Draft state ("To Submit") in order to confirm it.'))
        self.write({'state': 'send'})
        if self.type == 'remove' and self.employee_id.holiday_day_count < self.number_of_hours_temp:
            raise UserError(u'Хуримтлагдсан цаг байхгүй байна')
        if self.type == 'add' and  self.add_totol_moncon_hr_holidays + self.number_of_hours_temp > self.employee_id.contract_id.moncon_nohon_holiday_limit_day:
            raise UserError(u"Тухайн сарын нөхөн амрах цаг хэтэрэсэн байна %s!" % self.add_totol_moncon_hr_holidays)

        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'moncon.hr.holidays.workflow.history', 'history', self, self.employee_id.id)
            if success:
                if sub_success:
                    self.state = 'send'
                else:
                    self.check_sequence = current_sequence
        else:
            raise UserError('Ажлын урсгал үүсээгүй байна')
        if self.type == 'money':
            if self.number_of_hours_temp > self.add_totol_moncon_hr_holidays:
                raise UserError('Тухайн сарын нийт нөхөн амралт хуримтлуулсан цаг хүрэлцэхгүй байна!.')
            elif self.number_of_hours_temp < self.add_totol_moncon_hr_holidays:
                if self.number_of_hours_temp > self.total_moncon_holiday_limit:
                    raise UserError('Тухайн сарын хүсэлтийн цаг боломжит цагаас хэтэрсэн байна.')
        return True

    @api.multi
    def action_approve(self):
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'moncon.hr.holidays.workflow.history', 'history', self, self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                    self.register()
                else:
                    self.check_sequence = current_sequence

    # @api.multi
    # def action_refuse(self):
    #     if self.workflow_id:
    #         success = self.env['workflow.config'].reject('hr.holidays.workflow.history', 'history', self, self.env.user.id)
    #         if success:
    #             self.state = 'refuse'
    #
    #     for holiday in self:
    #         holiday.write({'state': 'refuse'})
    #
    #     return True

    @api.multi
    def _is_workflow_history(self):
        for obj in self:
            is_workflow = False
            user_id = self._uid
            for line in obj.history_lines:
                if user_id == line.user_id.id:
                    is_workflow = True
            obj.is_workflow_history = is_workflow

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['moncon.hr.holidays.workflow.history']
            validators = history_obj.search([('history', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1,
                                            order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.onchange('date_to', 'type')
    def _onchange_date_to(self):
        date_from = self.date_from
        date_to = self.date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.type == 'add' or self.type == 'money':
                if get_day_like_display(date_from, self.env.user).hour >= 6 and get_day_like_display(date_from,
                                                                                                     self.env.user).hour <= 12 and get_day_like_display(date_to,
                                                                                                                                                        self.env.user).hour >= 15 and get_day_like_display(
                        date_to, self.env.user).hour <= 20:
                    from_dt = fields.Datetime.from_string(date_from)
                    to_dt = fields.Datetime.from_string(date_to)
                    time_delta = to_dt - from_dt
                    total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600 - 1
                    self.number_of_hours_temp = total_hours
                    self.number_of_days_temp = total_hours / 8
                else:
                    if (date_to and date_from) and (date_from <= date_to):
                        if self.type == 'add' or self.type == 'money':
                            from_dt = fields.Datetime.from_string(date_from)
                            to_dt = fields.Datetime.from_string(date_to)
                            time_delta = to_dt - from_dt
                            total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                            self.number_of_hours_temp = total_hours
                            self.number_of_days_temp = total_hours / 8
            else:
                if (date_to and date_from) and (date_from <= date_to):
                    self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
                    self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
                else:
                    self.number_of_days_temp = 0
                    self.number_of_hours_temp = 0

    @api.multi
    @api.onchange('date_from')
    def _compute_check_in(self):
        for obj in self:
            if obj.date_from:
                start = datetime.strptime(obj.date_from, '%Y-%m-%d %H:%M:%S').date()
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(obj.employee_id, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.check_out = attendance.check_out

    @api.multi
    def register(self):
        for obj in self:
            if self.type=='add':
                self.employee_id.holiday_day_count += self.number_of_hours_temp
            else:
                self.employee_id.holiday_day_count -= self.number_of_hours_temp
            if obj.date_from and obj.date_to:
                check_date_start = datetime.strptime(obj.date_from, '%Y-%m-%d %H:%M:%S').replace(hour=00, minute=00, second=00)
                check_date_end = datetime.strptime(obj.date_from, '%Y-%m-%d %H:%M:%S').replace(hour=23, minute=59, second=59)
                attendances = self.env['hr.attendance'].sudo().search([('employee_id', '=', obj.employee_id.id), ('check_in', '>=', str(check_date_start)), ('check_out', '<=', str(check_date_end))])
                for attendance in attendances:
                    if attendance.switcher == True:
                        attendance.switcher = False
                    else:
                        attendance.switcher = True

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft', 'check_sequence': 0, 'workflow_id': self.workflow_id.id})


    def _get_number_of_days(self, date_from, date_to, employee_id):
        if self.employee_id.contract_id.working_hours:
            day_count = 0

            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_count += 1
            return day_count
        else:

            """ Returns a float equals to the timedelta between two dates given as string."""
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            day_count = 0
            if self.env.user.company_id.overtime_holiday:
                days = {'mon': 0, 'tue': 1, 'wed': 2, 'thu': 3, 'fri': 4, 'sat': 5, 'sun': 6}
                delta_day = timedelta(days=1)
                while from_dt <= to_dt:
                    if from_dt.weekday() in (days['sat'], days['sun']):
                        day_count += 1
                    else:
                        holidays = self.env['hr.holidays.configuration'].search([('date', '=', from_dt)])
                        if holidays:
                            day_count += 1
                    from_dt += delta_day

            return time_delta.days - day_count

    # Цагын зөрүүг өгөх функц
    def _get_number_of_hours(self, date_from, date_to):
        total_hours = 0.0
        if self.employee_id.contract_id.working_hours:
            st_date = get_day_by_user_timezone(date_from, self.env.user)
            end_date = get_day_by_user_timezone(date_to, self.env.user)
            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_start_dt = day.replace(hour=0, minute=0, second=0)
                day_end_dt = day.replace(hour=23, minute=59, second=59)
                if day.date() == st_date.date():
                    start_date = st_date
                else:
                    start_date = day_start_dt
                if day.date() == end_date.date():
                    finish = end_date
                else:
                    finish = day_end_dt
                resource_calendar = self.employee_id.contract_id.working_hours
                hours = resource_calendar.get_working_hours_of_date(start_dt=start_date, end_dt=finish,
                                                                    compute_leaves=True, resource_id=None,
                                                                    default_interval=None)
                total_hours += hours
        else:
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            total_hours = float(time_delta.seconds) / 3600
        return total_hours


class ExpenseWorkflowHistory(models.Model):
    _name = 'moncon.hr.holidays.workflow.history'
    _order = 'history, sent_date'

    STATE_SELECTION = [('waiting', 'Waiting'),
                       ('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('return', 'Return'),
                       ('rejected', 'Rejected')]
    history = fields.Many2one('moncon.hr.holidays', 'Expense', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_moncon_holidays_workflow_history_ref', 'history', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    holiday_day_count = fields.Float(u'Хуримтлагдсан цаг', track_visibility='onchange')
    holiday_day_display =  fields.Float(u'Хуримтлагдсан цаг', compute='compute_holiday_day_display')

    @api.multi
    def compute_holiday_day_display(self):
        self.holiday_day_display = self.holiday_day_count


    @api.multi
    def stat_button_hr_holidays(self):
        #  Тухайн ажилтаны хуримтлагдсан цаг

        for line in self:
            res = self.env['moncon.hr.holidays'].search([('employee_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': u'Хуримтлагдсан цаг',
                'res_model': 'moncon.hr.holidays',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }
