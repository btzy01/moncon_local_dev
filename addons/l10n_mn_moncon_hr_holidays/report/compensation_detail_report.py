# -*- encoding: utf-8 -*-
from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class CompensationDetailReport(models.TransientModel):
    """
        Нөхөн амралтын дэлгэрэнгүй тайлан
    """
    _name = 'compensation.detail.report'
    _description = "Compensation Detail Report"

    company_id = fields.Many2one('res.company', string="Компани", default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string="Эхлэх огноо", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date(string="Дуусах огноо", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    employee_id = fields.Many2one('hr.employee', string="Ажилтан", required=True)
    is_approved = fields.Boolean(string='Зөвшөөрсөн', default=True)

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Нөхөн амралтын дэлгэрэнгүй тайлан')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_title_left = {
            'font_name': 'Times New Roman',
            'font_size': 12,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#C9E3F3',
        }
        format_title_left = book.add_format(format_title_left)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('compensation_detail_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)

        # create name
        sheet.merge_range(rowx, 0, rowx, 4, '%s: %s' % (_('Компани'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range('A4:G5', report_name.upper(), format_name)

        sheet.merge_range('C7:E7', 'Хэлтэс: %s' % self.employee_id.job_id.name, format_filter)
        sheet.merge_range('C8:E8', 'Албан тушаал: %s' % self.employee_id.department_id.name, format_filter)
        sheet.merge_range('C9:E9', 'Овог: %s' % self.employee_id.last_name, format_filter)
        sheet.merge_range('C10:E10', 'Нэр: %s' % self.employee_id.name, format_filter)
        rowx += 9

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Хугацаа'), self.date_from, self.date_to),
                          format_filter)
        sheet.merge_range(rowx, 5, rowx, 6, '%s: %s' % (_('Хэвлэсэн огноо'), time.strftime('%Y-%m-%d')),
                          format_filter_right)
        rowx += 1
        sheet.write('A13', _('№'), format_title)
        sheet.write('B13', _('Огноо'), format_title)
        sheet.write('C13', _('Тайлбар'), format_title)
        sheet.write('D13', _('Төрөл'), format_title)
        sheet.write('E13', _('Нэмэгдсэн'), format_title)
        sheet.write('F13', _('Хасагдсан'), format_title)
        sheet.write('G13', _('Үлдэгдэл'), format_title)
        sheet.merge_range('A14:F14', _('Эхний үлдэгдэл'), format_title_left)
        rowx += 2
        add = 0
        not_add = 0
        if self.is_approved:
            holiday_ids = self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                                 ('date_from', '>=', self.date_from),
                                                                 ('date_from', '<=', self.date_to),
                                                                 ('state', '=', 'approved')], order='date_from')
            for l in self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                                   ('date_from', '<', self.date_from),
                                                                   ('type', '!=', 'add'),
                                                                   ('state', '=', 'approved')], order='date_from DESC'):
                not_add += l.number_of_hours_temp
            for li in self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                                   ('date_from', '<', self.date_from),
                                                                   ('type', '=', 'add'),
                                                                   ('state', '=', 'approved')], order='date_from'):
                add += li.number_of_hours_temp
        else:
            holiday_ids = self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                                 ('date_from', '>=', self.date_from),
                                                                 ('date_from', '<=', self.date_to)], order='date_from')
            for l in self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                            ('date_from', '<', self.date_from),
                                                            ('type', '!=', 'add')], order='date_from DESC'):
                not_add += l.number_of_hours_temp
            for li in self.env['moncon.hr.holidays'].search([('employee_id', '=', self.employee_id.id),
                                                             ('date_from', '<', self.date_from),
                                                             ('type', '=', 'add')], order='date_from'):
                add += li.number_of_hours_temp
        first_initial = add - not_add

        seq = 1
        rowi = 15
        last_initial = ''
        for line in holiday_ids:
            sheet.write('G14', first_initial, format_title_left)
            sheet.write(rowx, 0, seq, format_content_number)
            sheet.write(rowx, 1, line.date_from, format_content_text)
            sheet.write(rowx, 2, line.name, format_content_text)
            sheet.write(rowx, 3, dict(line._fields['type'].selection).get(line.type), format_content_text)
            sheet.write(rowx, 4, line.number_of_hours_temp if line.type == 'add' else 0, format_content_float)
            sheet.write(rowx, 5, line.number_of_hours_temp if line.type != 'add' else 0, format_content_float)
            if line.type == 'add':
                initial = '+%s' % line.number_of_hours_temp
            else:
                initial = '-%s' % line.number_of_hours_temp
            sheet.write_formula(rowx, 6, '{=G%s%s}' % (rowi-1, initial), format_content_float)
            last_initial = initial
            rowx += 1
            rowi += 1
            seq += 1

        sheet.merge_range(rowx, 0, rowx, 3, 'НИЙТ', format_title_left)
        sheet.write_formula(rowx, 4, '{=sum(E15:E%s)}' % (rowx), format_title_left)
        sheet.write_formula(rowx, 5, '{=sum(F15:F%s)}' % (rowx), format_title_left)
        sheet.write_formula(rowx, 6, '{=G%s}' % (rowx), format_title_left)
        rowx += 2
        sheet.merge_range(rowx, 2, rowx, 4, 'Бэлтгэсэн..................../Хүний нөөцийн менежер/', format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()