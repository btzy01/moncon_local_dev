# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import time
import logging
from odoo.exceptions import UserError
import sys
reload(sys)

_logger = logging.getLogger(__name__)


class WorkflowConfig(models.Model):
    """ Ажлын урсгалын тохиргоо """
    _inherit = 'workflow.config'



    workflow_type = fields.Selection([('department', 'Department'), ('tag', 'Tag')], string='Workflow type', required=True, default='department', track_visibility='onchange')
    workflow_tag_id = fields.Many2one('department.hr.tag',  string="Tags")

    @api.multi
    def write(self, vals):
        # Тухайн Бүтэц дээр ажлын урсгал өмнө нь үүссэн байвал анхааруулга өгнө.
        if vals.get('company_id') is not False or vals.get('company_id') is not None:
            com_id = self.env['res.company'].search(
                [('id', '=', vals['company_id'])]) if 'company_id' in vals else self.company_id
            if com_id.is_workflows:
                if vals.get('workflow_tag_id') is not False or vals.get('workflow_tag_id') is not None:
                    workflow_tag_id = vals.get('workflow_tag_id') if 'workflow_tag_id' in vals else self.workflow_tag_id.id
                if vals.get('model_id') is not False or vals.get('model_id') is not None:
                    model_id = vals.get('model_id') if 'model_id' in vals else self.model_id.id
                company_id = vals.get('company_id') if 'company_id' in vals else self.company_id.id

                if vals.get('workflow_tag_id'):
                    self._cr.execute(""" SELECT name
                                    FROM workflow_config
                                    WHERE workflow_tag_id  = %s and company_id = %s and model_id = %s and id != %s
                                    """, (workflow_tag_id, company_id, model_id, self.id))

                    workflow_config = self._cr.fetchall()
                    if len(workflow_config) != 0:
                        desc = _("The workflow config is registered in the system. \n Please check workflow config!")
                        raise UserError(desc)
        return super(WorkflowConfig, self).write(vals)

    @api.model
    def create(self, vals):
        # Тухайн Бүтэц дээр ажлын урсгал өмнө нь үүссэн байвал анхааруулга өгнө.
        if vals.get('company_id') is not False or vals.get('company_id') is not None:
            com_id = self.env['res.company'].search([('id', '=', vals['company_id'])])
            if com_id.is_workflows:
                if vals.get('workflow_tag_id') is not False or vals.get('workflow_tag_id') is not None:
                    workflow_tag_id = vals.get('workflow_tag_id')
                if vals.get('model_id') is not False or vals.get('model_id') is not None:
                    model_id = vals.get('model_id')
                company_id = vals.get('company_id')
                if vals.get('workflow_tag_id'):
                    self._cr.execute(""" SELECT name
                                    FROM workflow_config
                                    WHERE workflow_tag_id  = %s and company_id = %s and model_id = %s
                                    """, (workflow_tag_id, company_id, model_id))
                    workflow_config = self._cr.fetchall()
                    if len(workflow_config) != 0:
                        desc = _("The workflow config is registered in the system. \n Please check workflow config!")
                        raise UserError(desc)
        return super(WorkflowConfig, self).create(vals)


    @api.onchange('workflow_tag_id', 'model_id')
    def onchange_workflow_tag_id_type(self):
        if self.workflow_tag_id and self.model_id:
            self.update({'name': u'%s бүтцийн %s обектийн урсгал тохиргоо.' % (self.workflow_tag_id.name, self.model_id.name)})



    """ Ажлын урсгалыг олох функц
        type: employee, department гэсэн утгуудын аль нэгийг дамжуулна. Жишээ нь ажлын урсгалыг employee-оор олох бол employee утга дамжуулна
        model: Ажлын урсгалыг ашиглаж буй моделийн нэрийг  string-ээр дамжуулна
        employee_id: type нь employee гэж дамжуулсан бол энэ талбарыг заавал цэнэглэнэ. employee id-г дамжуулна. hr.employee
        department_id: type нь department гэж дамжуулсан бол энэ талбарыг заавал цэнэглэнэ. department id-г дамжуулна. hr.department"""
    @api.multi
    def get_workflow(self, wtype, model, employee_id=None, department_id=None):
        employee_obj = self.env['hr.employee']
        model_id = self.env['ir.model'].search([('model', '=', model)])
        department_obj = self.env['hr.department']
        workflow_obj = self.env['workflow.config']

        if not model_id:
            raise UserError(_('There is no model defined for (%s)!' % model))

        workflow_id = False

        if wtype == 'employee':
            employee = employee_obj.search([('id', '=', employee_id)])
            if employee:
                if employee.department_id:
                    """ажилтаны хэлтэсийг авч байна"""
                    department = employee.department_id
                    """Хэлтэсээр дээш гүйгээд хэлтэс тохируулсан тохиргоог олох хүртэл эсвэл хамгийн дээд талын эцэг хэлтэ олох хүртэл давтана"""
                    while 1 == 1:
                        workflow_ids = workflow_obj.search([('model_id', '=', model_id.id), ('department_id', '=', department.id)], limit=1)
                        if workflow_ids:
                            workflow_id = workflow_ids.id
                            break
                        else:
                            department_ids = department_obj.search([('id', '=', department.id)], limit=1)
                            if department_ids.parent_id:
                                department = department_ids.parent_id
                            else:
                                break
                else:
                    raise UserError(_('There is no department defined for employee.'))
            else:
                raise UserError(_('There is no employee defined for this user.'))
        elif wtype == 'department':
            department = department_obj.search([('id', '=', department_id)])
            """Хэлтэсээр дээш гүйгээд хэлтэс тохируулсан тохиргоог олох хүртэл эсвэл хамгийн дээд талын эцэг хэлтэ олох хүртэл давтана"""
            while 1 == 1:
                workflow_ids = workflow_obj.search([('model_id', '=', model_id.id), ('department_id', '=', department.id)], limit=1)
                if workflow_ids:
                    workflow_id = workflow_ids.id
                    break
                else:
                    department_ids = department_obj.search([('id', '=', department.id)])
                    if department_ids.parent_id:
                        department = department_ids.parent_id
                    else:
                        break
        if wtype == 'tag':

            employee = employee_obj.search([('id', '=', employee_id)])
            if employee:
                if employee.department_hr_tag_ids:
                    """ажилтаны бүтцийг авч байна"""
                    tag = employee.department_hr_tag_ids
                    """Бүтцээр дээш гүйгээд хэлтэс тохируулсан тохиргоог олох хүртэл эсвэл хамгийн дээд талын эцэг бүтцийг олох хүртэл давтана"""
                    workflow_ids = workflow_obj.search([('model_id', '=', model_id.id), ('workflow_tag_id', '=', tag.id)], limit=1)
                    if workflow_ids:
                        workflow_id = workflow_ids.id
                else:
                    raise UserError(_('There is no department defined for employee.'))
        return  workflow_id
        """Буцаах утгууд
            workflow_id: Ажлын урсгалыг буцаана, олдохгүй бол False :
        """
