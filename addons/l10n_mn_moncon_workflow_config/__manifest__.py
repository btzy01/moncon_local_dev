# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - workflow module',
    'version': '1.0',
    'author': 'Moncon LLC',
    "description": """
     Уг модуль нь Монконы ерөнхий Ажлын урсгалын модуль болно.
 """,
    'website': 'http://www.moncon.erp.mn',
    'images': [''],
    'depends': ['l10n_mn_workflow_config'],
    'data': [
        'views/workflow_config_view.xml'
    ],
    'installable': True,
    'auto_install': False
}
